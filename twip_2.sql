-- phpMyAdmin SQL Dump
-- version 4.7.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 27, 2019 at 04:12 PM
-- Server version: 5.6.35
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `twip_2`
--

-- --------------------------------------------------------

--
-- Table structure for table `adresse`
--

CREATE TABLE `adresse` (
  `id` int(11) NOT NULL,
  `societe_id` int(11) DEFAULT NULL,
  `zone_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jourDisponible` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `adresse`
--

INSERT INTO `adresse` (`id`, `societe_id`, `zone_id`, `name`, `jourDisponible`) VALUES
(1, 12, 2, 'Rue bourhuiba Mnihla 7015', 'a:2:{i:0;i:5;i:1;i:2;}'),
(3, 15, 1, 'Morouj 2 20932', 'a:1:{i:1;i:3;}'),
(4, 7, 1, 'Cité Masr Tunis', 'a:3:{i:0;i:1;i:1;i:3;i:2;i:4;}'),
(5, 7, 1, 'Cité nasser Tunis', 'a:1:{i:1;i:3;}'),
(6, 12, 1, 'Lac2, rue haroun', 'a:1:{i:0;i:1;}');

-- --------------------------------------------------------

--
-- Table structure for table `coupon`
--

CREATE TABLE `coupon` (
  `id` int(11) NOT NULL,
  `valeur` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_used` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `coupon`
--

INSERT INTO `coupon` (`id`, `valeur`, `code`, `is_used`) VALUES
(2, '16', '14674', 0),
(3, '10', '0', 1),
(4, '12', 'A1234', 1);

-- --------------------------------------------------------

--
-- Table structure for table `fos_user`
--

CREATE TABLE `fos_user` (
  `id` int(11) NOT NULL,
  `societe_id` int(11) DEFAULT NULL,
  `adresse_id` int(11) DEFAULT NULL,
  `gerant_id` int(11) DEFAULT NULL,
  `zone_id` int(11) DEFAULT NULL,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `nom` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `societe_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telephone` int(11) DEFAULT NULL,
  `receive_sms` tinyint(1) NOT NULL,
  `receive_mail` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fos_user`
--

INSERT INTO `fos_user` (`id`, `societe_id`, `adresse_id`, `gerant_id`, `zone_id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`, `nom`, `societe_name`, `telephone`, `receive_sms`, `receive_mail`) VALUES
(4, NULL, NULL, NULL, NULL, 'Admin', 'admin', 'admin@gmail.com', 'admin@gmail.com', 1, NULL, '$2y$13$bFEjllUv4E2GdOn8cTh03OcUtMTjECIF35a9Q4BBCQ90uwkT8lkiK', '2019-11-26 09:37:19', NULL, NULL, 'a:1:{i:0;s:10:\"ROLE_ADMIN\";}', '', NULL, NULL, 0, 0),
(96, NULL, NULL, NULL, NULL, 'gerant@gmail.com', 'gerant@gmail.com', 'gerant@gmail.com', 'gerant@gmail.com', 1, NULL, '$2y$13$RbCkjf/QXyS91mRbsqHOGOO56/W09J/gZl3Hgu6EgLeffWdUFCjiu', '2019-11-27 16:10:40', NULL, NULL, 'a:1:{i:0;s:11:\"ROLE_GERANT\";}', 'gerant twip', NULL, NULL, 0, 0),
(101, NULL, NULL, NULL, NULL, 'adnen.gerant@gmail.com', 'adnen.gerant@gmail.com', 'adnen.gerant@gmail.com', 'adnen.gerant@gmail.com', 1, NULL, '$2y$13$5PtVBI6ttbo..idr0EAbq.4FKHNJ2SQzHLErfqt5.XoI4pQo5IKKK', NULL, NULL, NULL, 'a:1:{i:0;s:11:\"ROLE_GERANT\";}', 'gerant 2 twip', NULL, NULL, 0, 0),
(102, NULL, NULL, NULL, NULL, 'adnen_gerant@gmail.com', 'adnen_gerant@gmail.com', 'adnen_gerant@gmail.com', 'adnen_gerant@gmail.com', 1, NULL, '$2y$13$Z.0Rk1.mSpC9fGZ9.emI3./Xc9yP2p0xS7PgywYezQYWFJnoOePBW', NULL, NULL, NULL, 'a:1:{i:0;s:11:\"ROLE_GERANT\";}', 'Adnen chouibi', NULL, NULL, 0, 0),
(103, 12, 1, NULL, NULL, 'adnen@app4mob.net', 'adnen@app4mob.net', 'adnen@app4mob.net', 'adnen@app4mob.net', 1, NULL, '$2y$13$ODiw1Q5uK7oYTTcm8ZOzmeD7cAG11UndXMCChG164QhWsxS7O0KW2', '2019-11-25 00:21:07', NULL, NULL, 'a:1:{i:0;s:11:\"ROLE_CLIENT\";}', 'Adnen chouibi', 'app4mob', 28226206, 1, 0),
(104, NULL, NULL, NULL, NULL, 'hichem@gmail.com', 'hichem@gmail.com', 'hichem@gmail.com', 'hichem@gmail.com', 0, NULL, '$2y$13$wHPIgEpHw9dpfUZPVEpE/.P.pw4ip1O5QBGAWjrOmaJsikpnxNVI.', NULL, '6lFJJ6LRpPHZFiecJhTjyiUPSXQdEDgfjRztdWo6Loc', NULL, 'a:1:{i:0;s:11:\"ROLE_CLIENT\";}', NULL, 'app4mob', NULL, 0, 0),
(105, NULL, NULL, 96, 2, 'operateur@gmail.com', 'operateur@gmail.com', 'operateur@gmail.com', 'operateur@gmail.com', 1, NULL, '$2y$13$AAN8I9is5gSiQqdJ9AwGE.FbQDUv/1MeYAWkaosOWFgB6FDYFWZPG', NULL, NULL, NULL, 'a:1:{i:0;s:14:\"ROLE_OPERATEUR\";}', 'Operateur', '', NULL, 0, 0),
(106, NULL, NULL, 96, 1, 'employee@gmail.com', 'employee@gmail.com', 'employee@gmail.com', 'employee@gmail.com', 1, NULL, '$2y$13$MDEQpdqB3A9zGwnd5NFYheefBHrKnuz/AkXujD9qEjxECHSTA8Ebe', NULL, NULL, NULL, 'a:1:{i:0;s:14:\"ROLE_OPERATEUR\";}', 'employee', '', NULL, 0, 0),
(107, NULL, NULL, 96, 1, 'operateur01@gmail.com', 'operateur01@gmail.com', 'operateur01@gmail.com', 'operateur01@gmail.com', 1, NULL, '$2y$13$nN0wwWqJYAYu3QA3kg1VSuAeEnA1XgF9MczwknOdaM0UcmeGAOFKm', '2019-11-26 14:19:04', NULL, NULL, 'a:1:{i:0;s:14:\"ROLE_OPERATEUR\";}', 'operateur01', '', NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `lavage`
--

CREATE TABLE `lavage` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `miniPrix` int(11) NOT NULL,
  `miniTemps` int(11) NOT NULL,
  `citadinePrix` int(11) NOT NULL,
  `citadineTemps` int(11) NOT NULL,
  `monoPrix` int(11) NOT NULL,
  `monoTemps` int(11) NOT NULL,
  `camionPrix` int(11) NOT NULL,
  `camionTemps` int(11) NOT NULL,
  `motoPrix` int(11) NOT NULL,
  `motoTemps` int(11) NOT NULL,
  `activation` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `lavage`
--

INSERT INTO `lavage` (`id`, `name`, `description`, `miniPrix`, `miniTemps`, `citadinePrix`, `citadineTemps`, `monoPrix`, `monoTemps`, `camionPrix`, `camionTemps`, `motoPrix`, `motoTemps`, `activation`) VALUES
(3, 'Lavage complet intérieur extérieur', NULL, 2, 5, 7, 10, 9, 60, 13, 100, 4, 20, 1),
(4, 'Majoration véhicules très sale', NULL, 5, 15, 10, 20, 15, 30, 40, 60, 0, 0, 1),
(5, 'Forfait avant revente ou restitution  véhicule', 'description lavage avant revente', 0, 0, 0, 0, 0, 0, 0, 0, 20, 30, 1);

-- --------------------------------------------------------

--
-- Table structure for table `produit`
--

CREATE TABLE `produit` (
  `id` int(11) NOT NULL,
  `libelle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `decription` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prix` double DEFAULT NULL,
  `image` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `produit`
--

INSERT INTO `produit` (`id`, `libelle`, `decription`, `prix`, `image`, `updated_at`) VALUES
(2, 'Total Quartz 9000', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has su', 30, '0001.jpg', '2019-10-18 18:10:28'),
(3, 'Brosse poils d\'animaux Total Wash', NULL, 5.5, '002.jpg', '0000-00-00 00:00:00'),
(4, 'Brosse de lavage pour jantes Total Wash', NULL, 5.5, '003.jpg', '0000-00-00 00:00:00'),
(5, 'Gant de lavage double face Total Wash', NULL, 5.5, '004.jpg', '0000-00-00 00:00:00'),
(6, 'Bombe anti-crevaison 500 ml', NULL, 15.5, '005.jpg', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `reservation`
--

CREATE TABLE `reservation` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `societe_id` int(11) DEFAULT NULL,
  `adresse_id` int(11) DEFAULT NULL,
  `zone_id` int(11) DEFAULT NULL,
  `coupon_id` int(11) DEFAULT NULL,
  `total_time` int(11) NOT NULL,
  `total_price` int(11) NOT NULL,
  `etat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `reservation`
--

INSERT INTO `reservation` (`id`, `user_id`, `societe_id`, `adresse_id`, `zone_id`, `coupon_id`, `total_time`, `total_price`, `etat`, `time`, `date`, `created_at`) VALUES
(1, 103, 12, 1, 2, NULL, 90, 24, '0', '08:00', '2020-02-14', '2019-11-25 00:35:34'),
(2, 103, 12, 1, 2, NULL, 120, 41, '1', '11:30', '2020-02-14', '2019-11-25 00:54:00');

-- --------------------------------------------------------

--
-- Table structure for table `reservation_has_voitures`
--

CREATE TABLE `reservation_has_voitures` (
  `id` int(11) NOT NULL,
  `reservation_id` int(11) NOT NULL,
  `voiture_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `reservation_has_voitures`
--

INSERT INTO `reservation_has_voitures` (`id`, `reservation_id`, `voiture_id`) VALUES
(1, 1, 17),
(2, 2, 17),
(3, 2, 16);

-- --------------------------------------------------------

--
-- Table structure for table `reservation_has_voitures_has_services`
--

CREATE TABLE `reservation_has_voitures_has_services` (
  `id` int(11) NOT NULL,
  `reservation_has_voitures_id` int(11) NOT NULL,
  `lavage_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `reservation_has_voitures_has_services`
--

INSERT INTO `reservation_has_voitures_has_services` (`id`, `reservation_has_voitures_id`, `lavage_id`) VALUES
(1, 1, 3),
(2, 1, 4),
(3, 2, 3),
(4, 2, 4),
(5, 3, 3),
(6, 3, 4),
(7, 3, 5);

-- --------------------------------------------------------

--
-- Table structure for table `societe`
--

CREATE TABLE `societe` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remise` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `societe`
--

INSERT INTO `societe` (`id`, `nom`, `mail`, `remise`) VALUES
(7, 'total', 'total.com', 10),
(8, 'total', NULL, 0),
(9, 'jklutl', NULL, 0),
(11, 'total8', NULL, 0),
(12, 'app4mob', 'app4mob.net', 10),
(13, 'total', 'total.com', 0),
(14, 'societe1', 'societe.tn', 0),
(15, 'app4mob bis', 'app4mob.mobi', 12);

-- --------------------------------------------------------

--
-- Table structure for table `user_has_lavages`
--

CREATE TABLE `user_has_lavages` (
  `id` int(11) NOT NULL,
  `lavage_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_has_lavages`
--

INSERT INTO `user_has_lavages` (`id`, `lavage_id`, `user_id`) VALUES
(11, 3, 96);

-- --------------------------------------------------------

--
-- Table structure for table `vehicule`
--

CREATE TABLE `vehicule` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `immatricule` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `modele` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `categorie` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updateAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `vehicule`
--

INSERT INTO `vehicule` (`id`, `user_id`, `immatricule`, `modele`, `type`, `categorie`, `updateAt`) VALUES
(16, 103, '172 tun 477', 'Polo7', '1', '0', '2019-11-25 00:22:59'),
(17, 103, '123 tun 131', 'BMW X6', '2', '1', '2019-11-25 00:22:59');

-- --------------------------------------------------------

--
-- Table structure for table `zone`
--

CREATE TABLE `zone` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `zone`
--

INSERT INTO `zone` (`id`, `name`) VALUES
(1, 'Tunis1'),
(2, 'Tunis2');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adresse`
--
ALTER TABLE `adresse`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_C35F0816FCF77503` (`societe_id`),
  ADD KEY `IDX_C35F08169F2C3FAB` (`zone_id`);

--
-- Indexes for table `coupon`
--
ALTER TABLE `coupon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fos_user`
--
ALTER TABLE `fos_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479C05FB297` (`confirmation_token`),
  ADD KEY `IDX_957A6479FCF77503` (`societe_id`),
  ADD KEY `IDX_957A64794DE7DC5C` (`adresse_id`),
  ADD KEY `IDX_957A6479A500A924` (`gerant_id`),
  ADD KEY `IDX_957A64799F2C3FAB` (`zone_id`);

--
-- Indexes for table `lavage`
--
ALTER TABLE `lavage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produit`
--
ALTER TABLE `produit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_42C84955A76ED395` (`user_id`),
  ADD KEY `IDX_42C84955FCF77503` (`societe_id`),
  ADD KEY `IDX_42C849554DE7DC5C` (`adresse_id`),
  ADD KEY `IDX_42C849559F2C3FAB` (`zone_id`),
  ADD KEY `IDX_42C8495566C5951B` (`coupon_id`);

--
-- Indexes for table `reservation_has_voitures`
--
ALTER TABLE `reservation_has_voitures`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_F82FBB2DB83297E7` (`reservation_id`),
  ADD KEY `IDX_F82FBB2D181A8BA` (`voiture_id`);

--
-- Indexes for table `reservation_has_voitures_has_services`
--
ALTER TABLE `reservation_has_voitures_has_services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_DFDE18E9FA345A51` (`reservation_has_voitures_id`),
  ADD KEY `IDX_DFDE18E9B2E54F9B` (`lavage_id`);

--
-- Indexes for table `societe`
--
ALTER TABLE `societe`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_has_lavages`
--
ALTER TABLE `user_has_lavages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_B52E6396B2E54F9B` (`lavage_id`),
  ADD KEY `IDX_B52E6396A76ED395` (`user_id`);

--
-- Indexes for table `vehicule`
--
ALTER TABLE `vehicule`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_292FFF1DA76ED395` (`user_id`);

--
-- Indexes for table `zone`
--
ALTER TABLE `zone`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adresse`
--
ALTER TABLE `adresse`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `coupon`
--
ALTER TABLE `coupon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `fos_user`
--
ALTER TABLE `fos_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;
--
-- AUTO_INCREMENT for table `lavage`
--
ALTER TABLE `lavage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `produit`
--
ALTER TABLE `produit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `reservation`
--
ALTER TABLE `reservation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `reservation_has_voitures`
--
ALTER TABLE `reservation_has_voitures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `reservation_has_voitures_has_services`
--
ALTER TABLE `reservation_has_voitures_has_services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `societe`
--
ALTER TABLE `societe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `user_has_lavages`
--
ALTER TABLE `user_has_lavages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `vehicule`
--
ALTER TABLE `vehicule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `zone`
--
ALTER TABLE `zone`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `adresse`
--
ALTER TABLE `adresse`
  ADD CONSTRAINT `FK_C35F08169F2C3FAB` FOREIGN KEY (`zone_id`) REFERENCES `zone` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `FK_C35F0816FCF77503` FOREIGN KEY (`societe_id`) REFERENCES `societe` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `fos_user`
--
ALTER TABLE `fos_user`
  ADD CONSTRAINT `FK_957A64794DE7DC5C` FOREIGN KEY (`adresse_id`) REFERENCES `adresse` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_957A64799F2C3FAB` FOREIGN KEY (`zone_id`) REFERENCES `zone` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `FK_957A6479A500A924` FOREIGN KEY (`gerant_id`) REFERENCES `fos_user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_957A6479FCF77503` FOREIGN KEY (`societe_id`) REFERENCES `societe` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `reservation`
--
ALTER TABLE `reservation`
  ADD CONSTRAINT `FK_42C849554DE7DC5C` FOREIGN KEY (`adresse_id`) REFERENCES `adresse` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `FK_42C8495566C5951B` FOREIGN KEY (`coupon_id`) REFERENCES `coupon` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_42C849559F2C3FAB` FOREIGN KEY (`zone_id`) REFERENCES `zone` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `FK_42C84955A76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_42C84955FCF77503` FOREIGN KEY (`societe_id`) REFERENCES `societe` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `reservation_has_voitures`
--
ALTER TABLE `reservation_has_voitures`
  ADD CONSTRAINT `FK_F82FBB2D181A8BA` FOREIGN KEY (`voiture_id`) REFERENCES `vehicule` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_F82FBB2DB83297E7` FOREIGN KEY (`reservation_id`) REFERENCES `reservation` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `reservation_has_voitures_has_services`
--
ALTER TABLE `reservation_has_voitures_has_services`
  ADD CONSTRAINT `FK_DFDE18E9B2E54F9B` FOREIGN KEY (`lavage_id`) REFERENCES `lavage` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_DFDE18E9FA345A51` FOREIGN KEY (`reservation_has_voitures_id`) REFERENCES `reservation_has_voitures` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_has_lavages`
--
ALTER TABLE `user_has_lavages`
  ADD CONSTRAINT `FK_B52E6396A76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_B52E6396B2E54F9B` FOREIGN KEY (`lavage_id`) REFERENCES `lavage` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `vehicule`
--
ALTER TABLE `vehicule`
  ADD CONSTRAINT `FK_292FFF1DA76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`) ON DELETE CASCADE;
