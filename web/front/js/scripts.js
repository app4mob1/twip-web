(function($){
  'use strict';

  /* Plugin variables */
  var pluginName, defaultOptions = {};

  /**
   * Constructor.
   */
  function Plugin(element, options) {
    // Merge specific and default options.
    this.options = $.extend({}, defaultOptions, options);

    // Initialize the main element.
    this.$element = (element instanceof $)? element: $(element);

    // Save the instance reference into the DOM element.
    this.$element.data(pluginName, this);

    // Object initialisation.
    if (typeof this.setup === 'function') {
      this.setup();
    }
    if (typeof this.bind === 'function') {
      this.bind();
    }
    if (typeof this.init === 'function') {
      this.init();
    }
  }


  /* Expose jQuery plugin. */
  $.fn[pluginName] = function(options) {
    return this.each(function() {
      var $this = $(this);
      if (!$this.data(pluginName)) {
        new Plugin($this, options);
      }
    });
  };

})(jQuery);
;(function($) {
  $.fn.loadlazy = function(threshold, callback) {
    $(this).each(function() {
      var $img = $(this);
      var src = $img.data('src');
      if (src) {
        $img.attr("src", src);
        if (typeof callback === "function") {
          callback.call(this);
        }
      }
    });
    return this;
  };

})(jQuery);

// appliquer le plugin sur les images voulues, apres chargement de page
jQuery(function() {
  jQuery('.loadlazy img').loadlazy();
});
(function ($) {
  'use strict';

  /* Plugin variables */
  var pluginName;
  var defaultOptions = {};

  /**
   * Constructor.
   * @param {Object} element
   * element
   * @param {Object} options
   * options
   */
  function Plugin(element, options) {
    // Merge specific and default options.
    this.options = $.extend({}, defaultOptions, options);

    // Initialize the main element.
    this.$element = (element instanceof $) ? element : $(element);

    // Save the instance reference into the DOM element.
    this.$element.data(pluginName, this);

    // Object initialisation.
    if (typeof this.setup === 'function') {
      this.setup();
    }
    if (typeof this.bind === 'function') {
      this.bind();
    }
    if (typeof this.init === 'function') {
      this.init();
    }
  }

  /* ********* Start plugin specific code ********* */

  /* Plugin name. */
  pluginName = 'mediaPopin';

  /* Plugin default options. */
  defaultOptions = {
    modal: true,
    width: '960px',
    baseUrl: '/ajax/display-full/',
    wrapperClass: 'js-inside-popin',
    sharethisBlockClass: 'sharethis-block-bottom-entity',
    activateZoom: false
  };

  /**
   * Setup plugin.
   * e.g. Get DOM elements, setup data...
   */
  Plugin.prototype.setup = function () {
    this.$popinWrapper = $('.' + this.options.wrapperClass);
    this.$popin = null;
    this.$popinContext = null;
  };




  /* ********* End plugin specific code ********* */

  /* Expose jQuery plugin. */
  $.fn[pluginName] = function (options) {
    return this.each(function () {
      var $this = $(this);
      if (!$this.data(pluginName)) {
        new Plugin($this, options);
      }
    });
  };
})(jQuery);


//dont touch menue
(function ($, window) {
  'use strict';

  $(function () {
    $(window).load(function () {
      var windowWidth = $(window).width();
      var $mainMenu = $('.main-menu');
      var $menuBtnOpen = $('.js-btn-open');
      var $menuBtnClose = $('.js-btn-close');
      var $blockSharing = $('.block-sharing-search');
      var $menuContent = $('.js-menu-content');
      var $hoverFlying = $('.hover-flying');
      var $hoverFlyingActive = $('.hover-flying-active');
      var $activeMainLink = $('.js-hover-flying-active');
      var menuOpenClass = 'menu-mob__open';
      var searchOpenClass = 'sharing-search__open';
      var activeClass = 'on';
      var hoverFlyingDuration = 300;
      var defaultHoverFlyingPosition = 0;
      var defaultHoverFlyingWidth = 0;
      var defaultHoverFlyingOpacity = 0;

      // Hover inner mega menu
      $('.mega-menu-sub').each(function () {
        var $defaultItem = $('.mega-menu-sub-item.js-default-active', this);
        $('.mega-menu-sub-item', this).hover(function () {
          $defaultItem.removeClass(activeClass);
          $(this).addClass(activeClass);
        }, function () {
          $(this).removeClass(activeClass);
          $defaultItem.addClass(activeClass);
          $(".diagonal-helper").removeAttr("style");
        });
      });
      $(".diagonal-helper").hover(function () {
        $(this).hide(1500);
      });
      $('.mega-menu-item').hover(function () {}, function () {
        $(".diagonal-helper").removeAttr("style");
      });

      // Menu mobile
      $menuBtnOpen.on('click tap taphold', function (e) {
        $(this).hide();
        $blockSharing.addClass(searchOpenClass);
        $menuBtnClose.show();
        $menuContent.slideDown();
        $mainMenu.addClass(menuOpenClass);
        e.preventDefault();
      });

      $menuBtnClose.on('click tap taphold', function (e) {
        $(this).hide();
        $menuBtnOpen.show();
        $mainMenu.removeClass(menuOpenClass);
        $blockSharing.removeClass(searchOpenClass);
        $menuContent.slideUp();
        e.preventDefault();
        return false;
      });

      /* Menu mobile */
      $(window).resize(function () {
        var newWindowWidth = jQuery(window).width();
        if (getScreenSizeMenu(newWindowWidth) !== getScreenSizeMenu(windowWidth)) {
          if ($(window).width() < 959) {
            $menuBtnClose.hide();
            $menuBtnOpen.show();
            $menuContent.slideUp('normal', function () {
              $mainMenu.removeClass(menuOpenClass);
            });
          }
          else {
            $menuContent.show();
          }
        }
        windowWidth = $(window).width();
      });

      function getScreenSizeMenu(resolution) {
        var result = 1;
        if (resolution >= 959) {
          result = 2;
        }
        return result;
      }

      // Hover flying
      setDefaultValuesHoverFlying();
      $('.js-hover-flying, .js-hover-flying-active').mouseenter(function () {
        setPositionHoverFlying($(this));
        if ($(this).find('.mega-menu-sub').length) {
          $('body').addClass('layered');
          $('.menu-overlay').show();
        }
      });
      $('.js-hover-flying, .js-hover-flying-active').mouseleave(function () {
        $('body').removeClass('layered');
        $('.menu-overlay').hide();
      });

      $menuContent.mouseleave(function () {
        $hoverFlying.stop().animate({
          opacity: defaultHoverFlyingOpacity,
          left: defaultHoverFlyingPosition,
          width: defaultHoverFlyingWidth
        }, hoverFlyingDuration);
      });

      /* Hover Flying functions */
      function setDefaultValuesHoverFlying() {
        if ($activeMainLink.length) {
          defaultHoverFlyingPosition = 0;
          defaultHoverFlyingWidth = 0;
          defaultHoverFlyingOpacity = 0;
          setPositionActiveHoverFlying($activeMainLink);
        }
      }

      function setPositionHoverFlying($link) {
        var values = calculatePositionHoverFlying($link);
        $hoverFlying.animate({
          opacity: 1,
          left: values.position,
          width: (values.width - 20)
        }, hoverFlyingDuration);
      }

      function setPositionActiveHoverFlying($link) {
        var values = calculatePositionHoverFlying($link);
        // Avoid weird apparition of normal border bar.
        $hoverFlying.animate({
          opacity: 0,
          left: values.position,
          width: (values.width - 20)
        }, hoverFlyingDuration);

        $hoverFlyingActive.animate({
          opacity: 1,
          left: values.position,
          width: (values.width - 20)
        }, hoverFlyingDuration);
      }

      var navbar = jQuery('.sliding-menu');
      var navbarHeight = navbar.offset()['top'];
      var sloganHeight = 37;
      var mobileHeaderBreakpoint = 960;
      var header = jQuery('header');

      function setStickyMenu() {
        var topOffset = jQuery(window).scrollTop();
        var windowWidth = jQuery(window).width();
        var activeMenu = jQuery('.js-hover-flying-active');
        if ((topOffset + sloganHeight >= navbarHeight || windowWidth < mobileHeaderBreakpoint) && !navbar.hasClass('sticky-menu')) {
          navbar.addClass('sticky-menu');
          if (activeMenu.length > 0) {
            jQuery('.hover-flying-active').animate({
              left: activeMenu.position().left + 100
            }, 300);
          }
        }
        else if ((topOffset + sloganHeight < navbarHeight || topOffset === 0) && navbar.hasClass('sticky-menu') && windowWidth >= mobileHeaderBreakpoint) {
          navbar.removeClass('sticky-menu');
          if (activeMenu.length > 0) {
            jQuery('.hover-flying-active').animate({
              left: activeMenu.position().left
            }, 300);
          }
        }
        if (!navbar.hasClass('sticky-menu')) {
          header.height(header.height());
        }
      }

      setStickyMenu();

      jQuery(window).scroll(setStickyMenu);

      function calculatePositionHoverFlying($link) {
        var width = $link.innerWidth();
        var position = $link.position().left;

        return {
          width: width,
          position: position
        };
      }
    });
  });
}(window.jQuery, window));




/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function charactersShowLimit(text) {
  if (jQuery(text).length) {
    var blocText = jQuery(text);
    if (blocText.text().length >= 200 && blocText.text().length < 300) {
      blocText.css('font-size', '26px');
    }
    else if (blocText.text().length < 200 && blocText.text().length >= 100) {
      blocText.css('font-size', '30px');
    } else {
      blocText.css('font-size', '34px');
    }
  }
}
//Limit display caracters of bloc exergue
jQuery(document).ready(function () {
  charactersShowLimit('.mega-gabarit-content .one-columns .block-exergue-fields__description p');
});


jQuery(window).scroll(animateKeyFigure);

function animateKeyFigure() {
  var nb_step = 40;
  var animation_duration = 3000; // milliseconds
  jQuery('.bean-key-figures__dynamic').each(function () {
    var selector = jQuery(this).find('.bean-key-figures--main-number');
    var selectorTopOffset = selector.offset().top;
    var selectorBottomOffset = selectorTopOffset + selector.height();
    var windowTopOffset = jQuery(window).scrollTop();
    var windowBottomOffset = windowTopOffset + jQuery(window).height();
    var number = selector.data('number');
    var roundMultiplier = 1;
    var nbDecimals = countDecimals(number);
    if (windowTopOffset < selectorTopOffset && windowBottomOffset > selectorBottomOffset) {
      var step = number / nb_step;

      // polyfill, Number.isNumber is part of ES6 so not supported by IE11
      Number.isInteger = Number.isInteger || function(value) {
        return typeof value === 'number' &&
            isFinite(value) &&
            Math.floor(value) === value;
      };

      if (!Number.isInteger(number)) {
        // This prevent numbers from being rounded with too much decimals.
        for (var i = 1; i <= nbDecimals; i++) {
          roundMultiplier *= 10;
        }
      }
      step = Math.round(step * roundMultiplier) / roundMultiplier;
      jQuery(this).removeClass('bean-key-figures__dynamic');
      if (number >= nb_step / 2) { // this test prevent number to stay at 0 because step would be rounded to 0.
        var currentNumber = 0;
        var displayedNumber = '0';
        selector.html(displayedNumber);
        var interval = setInterval(function () {
          currentNumber = Math.round((currentNumber + step) * roundMultiplier) / roundMultiplier;
          displayedNumber = currentNumber.toString();
          if (countDecimals(currentNumber) !== nbDecimals) {
            // This prevent numbers from being displayed with missing decimals which would render badly.
            var missingDecimals = nbDecimals - countDecimals(currentNumber);
            if (nbDecimals > 0 && countDecimals(currentNumber) === 0) {
              displayedNumber += '.';
            }
            for (var j = 1; j <= missingDecimals; j++) {
              displayedNumber += '0';
            }
          }
          if (number <= currentNumber) {
            clearInterval(interval);
            displayedNumber = number.toString();
          }
          selector.html(displayedNumber);
        }, animation_duration / nb_step);
      }
      else {
        selector.html(number);
      }
    }
  });
}

function countDecimals(number) {
  if (Math.floor(number) !== number) {
    return number.toString().split(".")[1].length || 0;
  }
  return 0;
}

jQuery(document).ready(function() {

  jQuery('.main-content').on('click', '.menu-toggable-facet h4', function() {
    var menuParent = jQuery(this).parent();
    if (menuParent.hasClass('facet-closed')) {
      jQuery('.facet-children-container', menuParent).stop().slideDown(300, function() {
        menuParent.removeClass('facet-closed');
      });
    } else {
      jQuery('.facet-children-container', menuParent).stop().slideUp(300, function() {
        menuParent.addClass('facet-closed');
      });
    }
  });

});
jQuery(document).ready(function () {

  /* Galleries */
  var showBtn = jQuery('.galleries .show-more .btn-show-more');
  showBtn.on('click', function () {
    var next = jQuery('.box-galleries:visible:last').next();
    next.show();
    if (jQuery('.box-galleries:hidden').length === 0) {
      showBtn.hide();
    }
  });
});

function sharePriceMarquee() {
  jQuery(".field-name-field-contenu-cours-de-bourse .field-items").marquee({
    delayBeforeStart: 1000,
    direction: 'left',
    duplicated: true,
    duration: 15000,
    gap: 20,
    pauseOnCycle: false,
    pauseOnHover: true
  });
}

jQuery(document).ready(function () {
  var matchMediaObject = window.matchMedia || window.msMatchMedia;
  if (jQuery('.field-name-field-contenu-cours-de-bourse .field-item').length > 1 ||
      (matchMediaObject && matchMediaObject('all and (max-width: 959px)').matches)) {
    sharePriceMarquee();
  }
});

/********** v3 *************/

var sliderActive = false;
var bigSliderActive = false;
var slider;

// function iOSversion() {
//   if (/iP(hone|od|ad)/.test(navigator.platform)) {
//     // supports iOS 2.0 and later: <http://bit.ly/TJjs1V>
//     var v = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/);
//     return [parseInt(v[1], 10), parseInt(v[2], 10), parseInt(v[3] || 0, 10)];
//   }
// }

// function enableTouchIfNotIOS7() {
//   var version = iOSversion();

//   if (version && version[0] > 6) {
//     return false;
//   } else {
//     return true;
//   }
// }

// var touchEnable = enableTouchIfNotIOS7();

function createSlider() {
  slider = jQuery('.mosaic-small-image .js-mosaic').bxSlider({
//    touchEnabled: touchEnable,
    adaptiveHeight: false,
    controls: true
  });
  return true;
}

function createBigSlider(sliderId) {
  slider = jQuery('#'+sliderId).bxSlider({
//    touchEnabled: touchEnable,
    adaptiveHeight: false,
    minSlides: 3,
    maxSlides: 3,
    slideWidth: 300,
    slideMargin : 30,
    infiniteLoop : false
  });
  return true;
}

//create slider if new page is wide
jQuery(document).ready(function(){

  if(jQuery('.mosaic .js-mosaic')[0]) {
    sliderActive = createSlider();

    jQuery('.big-slider').each( function() {
      if(jQuery(this).children().length > 4) {
        var sliderId = jQuery(this).attr('id');
        bigSliderActive = createBigSlider(sliderId);
      }
    });
  }


  jQuery('.big-slider img').click(function(){
    var element = jQuery(this).parent().parent();
    var currentSlider = element.parent();
    currentSlider.find('.item-mosaic').show();
    jQuery('#focus-'+currentSlider.attr('id')).html(element.html());
    //console.log(currentSlider.attr('id'));
    element.hide();
  });

  jQuery('.big-slider li:first-child').hide();

});

var tabsSocialActive = false;

function tabsSocial(){
  var windowWidth = jQuery(window).width();
  var windowWidthCase = (windowWidth > 959) || (windowWidth < 720);

  if (windowWidthCase) {
    jQuery('.btn-social.facebook').removeClass('hidden');
    jQuery('.btn-social.twitter').addClass('hidden');
  } else {
    jQuery('.btn-social.twitter').removeClass('hidden');
  }

  if (windowWidthCase && tabsSocialActive === false ) {
    jQuery(".js-tabs-social").tabs({
      beforeActivate: function(event, ui) {

        var $blockSocial = ui.oldPanel.parents('.block-social-news');
        if (ui.oldPanel.hasClass('block-facebook')) {
          $blockSocial.find('.btn-social.facebook').addClass('hidden');
          $blockSocial.find('.btn-social.twitter').removeClass('hidden');
        }
        else {
          $blockSocial.find('.btn-social.facebook').removeClass('hidden');
          $blockSocial.find('.btn-social.twitter').addClass('hidden');
        }
      }
    });
    tabsSocialActive = true;
  }
  else if(!windowWidthCase && tabsSocialActive === true){
    jQuery(".js-tabs-social").tabs( "destroy" );
    tabsSocialActive = false;
  }
}

jQuery(document).ready(function(){
  tabsSocial();
});

jQuery(window).resize(function() {
  tabsSocial();
});
jQuery(document).ready(function(){
  if (jQuery(window).width() < 719){
    jQuery(".block-overviews .js-accordion").accordion({
      collapsible: true,
      active: false
    });
  }else{
    jQuery(".block-overviews .js-tabs2").tabs();
  }
});


jQuery(window).resize(function() {
  if (jQuery(window).width() < 719){
    jQuery(".block-overviews .js-accordion").accordion({
      collapsible: true,
      active: false
    });
    jQuery(".block-overviews .js-tabs2").tabs("destroy");
    jQuery(".block-overviews .ui-accordion-content").removeAttr('style');
  }else{
    jQuery(".js-accordion").accordion( "destroy" );
    jQuery(".block-overviews .js-tabs2").tabs();
  }
});

(function ($) {
  "use strict";

  $(function () {
    var btnOpen = $('.js-btn-business');

    // For each btnOpen listen business-popin-$id and open dialog.
    btnOpen.each(function (index, business) {
      $(business).on('click tap taphold', function (event) {
        event.preventDefault();
        var bid = $(this).attr('data-bid');
        $('.js-business-popin-' + bid).dialog({
          width: '80vw',
          modal: true,
          resizable: false,
          dialogClass: 'modal-splash',
          open: function (event) {
            $('body').css({overflow: "hidden"});
            var overlay = $('.ui-widget-overlay');
            overlay.click(function () {
              $(event.target).dialog('close');
            });
          },
          close: function () {
            $('body').css({overflow: "auto"});
          }
        });
      });
    });
  });

})(jQuery);

/**
 * resize font size bloc key figure
 */
var ResizeFontKeyfigure = function (parentSelector, childSelector, fontSizeMapping) {
  if (jQuery(parentSelector).length) {
    jQuery(childSelector).each(function (i) {
      var fontSize;
      var parent = jQuery(this).closest('.js-number');
      var char = parent.html(jQuery(this)).text().trim();
      for (i in fontSizeMapping) {
        if (char.length > i) {
          fontSize = fontSizeMapping[i];
        }
      }
      jQuery(this).css('font-size', fontSize);
    });
  }
};
jQuery(document).ready(function () {

  /**
   * Change font-size for Key Figures until size of block 
   */
  if (window.innerWidth >= 720) {
    var fontSizeMapping3 = {"9": "26px", "7": "30px", "5": "38px", "4": "50px", "3": "67px", "2": "88px", "1": "110px"};
    new ResizeFontKeyfigure(
          '.bean-project .bean-figures .js-number > span',
          '.bean-project .bean-figures .js-number span',
          fontSizeMapping3
        );
  }
  if (jQuery('.cord-link').length) {
    jQuery('.field-name-field-location ').click(function () {
      var urlNode = jQuery('.cord-link').attr('href');
      window.open(urlNode, '_blank');
      jQuery('.cord-link').off('click').removeAttr('href');
    });
  }
});

(function ($) {
  "use strict";

  // The slider content of a popin is retrieve through an AJAX request.
  $(document).ajaxSuccess(function (event, request, settings) {
    var url = settings.url;
    var contain = 'ajax/display-full/bean';
    // AJAX url was for the render of a Bean entity.
    if (url.indexOf(contain) > -1) {
      $('.js-block-slider').wrapBxSlider();
    }
  });

  $(document).ready(function () {

    if (navigator.userAgent.match(/(android|iphone|blackberry|symbian|symbianos|symbos|netfront|model-orange|javaplatform|iemobile|windows phone|samsung|htc|opera mobile|opera mobi|opera mini|presto|huawei|blazer|bolt|doris|fennec|gobrowser|iris|maemo browser|mib|cldc|minimo|semc-browser|skyfire|teashark|teleca|uzard|uzardweb|meego|nokia|bb10|playbook)/gi)) {
      // Only if mobile device.
      var buttonsVideoOpen = $('.js-media-popin-link ');
      buttonsVideoOpen.on('click', function () {
        $('body').css("overflow", "hidden");
      });
    }

  });

})(jQuery);


(function ($, window) {
  $(function () {
    $(window).load(function () {
      if ($('.timeline-none-column .block-total-timeline').length && (jQuery(window).width() >= 720)) {
        $('.js-hover-periode').mouseenter(function () {
          setPositionHoverTimeLine($(this));
        }
        );
        $('#timeLineNav').mouseleave(function () {
          $('#nav-title').stop().animate({
            opacity: 0,
            left: 0,
            width: 0
          }, 400);
        });
      }
      function setPositionHoverTimeLine($link) {
        var values = calculatePositionHoverTimeLine($link);
        $('#nav-title').animate({
          opacity: 1,
          left: values.position,
          width: values.width
        }, 400);
      }
      function calculatePositionHoverTimeLine($link) {
        var width = $link.width();
        var position = $link.position().left + 20;
        if ($link.is(':first-child')) {
          position = $link.position().left+5;
          width = $link.width()+5;
        }
        return {
          'width': width-40,
          'position': position+10,
        };
      }
      $(".js-hover-periode").click(function () {
        var itemYear = $(this).text();
        if (itemYear.length > 4) {
          itemYear = itemYear.substr(0, 4);
        }
        var plusTop = 85;
        if (($('.timeline-with-column .block-total-timeline ').length) > 0) {
          plusTop = 330;
        }
        else if ((jQuery(window).width() < 720)) {
          plusTop = 400;
        }
        $('html, body').animate({
          scrollTop: $("#" + itemYear).offset().top - plusTop
        }, 900);
      });
      if ((jQuery(window).width() < 720) || ($('.timeline-with-column .block-total-timeline ').length)) {
        $("#timeLineNav").click(function () {
          $(this).toggleClass('active');
        });
      }
      if( jQuery('.timeline-none-column .block-total-timeline').length > 0) {
        var timeLine = jQuery('.timeline-none-column .block-total-timeline').offset().top;
        jQuery(document).scroll(function () {
          if (jQuery(document).scrollTop() >= timeLine && (jQuery(window).width() >= 720) && (jQuery('.timeline-with-column .block-total-timeline ').length === 0)) {
            jQuery('.timeline-none-column.block-total-timeline').addClass('fixed-nav');
          } else {
            jQuery('.timeline-none-column .block-total-timeline').removeClass('fixed-nav');
          }
        });
      }

    });
  });
}(window.jQuery, window));
function readMoreListe(itemsNumber){
  var number = itemsNumber;
  jQuery('.js-read-more-container .js-btn-read a').unbind('click');


  jQuery('.js-read-more-container .js-btn-read').addClass('read-more').removeClass('read-less');
  jQuery('.js-read-more-list > li').each(function(i){
    if (i > number){
      //jQuery(this).slideUp('fast');
      jQuery(this).css({'display' : 'none'});
    }
  });

  jQuery('.js-read-more-container .js-btn-read a').bind("click", function(){
    if (jQuery(this).parent().hasClass('read-more')){
      jQuery('.js-read-more-list > li').each(function(i){
        if (i > number){
          jQuery(this).slideDown('fast');
        }
      });
      jQuery(this).parent().addClass('read-less').removeClass('read-more');
    }else{
      jQuery('.js-read-more-list > li').each(function(i){
        if (i > number){
          jQuery(this).slideUp('fast');
        }
      });
      jQuery(this).parent().addClass('read-more').removeClass('read-less');
    }
  });

}

function getScreenSize(resolution) {
  var result = 1;
  if(resolution >= 480) {
    result = 2;
  }
  if(resolution >= 720) {
    result = 3;
  }
  return result;
}

jQuery(document).ready(function(){
  if (jQuery(window).width() < 480){
    if (jQuery('.block-websites-local').length !== 0){
      readMoreListe(5);
    }else {
      readMoreListe(1);
    }
  }else if (jQuery(window).width() < 720){
    if (jQuery('.block-websites-local').length !== 0){
      readMoreListe(5);
    }else {
      readMoreListe(2);
    }
  }


});
var lastWindowWidth = jQuery(window).width();
jQuery(window).resize(function(){

  var newWindowWidth = jQuery(window).width();
  if(getScreenSize(newWindowWidth) != getScreenSize(lastWindowWidth)) {
    if (jQuery(window).width() < 480){
      if (jQuery('.block-websites-local').length !== 0){
        readMoreListe(5);
      }else {
        readMoreListe(1);
      }
    }else if (jQuery(window).width() < 720){
      if (jQuery('.block-websites-local').length !== 0){
        readMoreListe(5);
      }else {
        jQuery(jQuery('.js-read-more-list > li')[2]).css('display', 'inline-block');
        readMoreListe(2);
      }
    }else{
      jQuery('.js-read-more-list > li').slideDown('fast');
      jQuery('.js-read-more-list > li').css('display', 'inline-block');
    }
  }
  lastWindowWidth = jQuery(window).width();
});
function tabPosition(){

  if (jQuery(window).width() < 719) {
    //width 720
    jQuery(".active-tab-mob").empty();
    jQuery(".worldwide-tabs .ui-tabs-active h3 a").clone().prependTo(".active-tab-mob");
    //jQuery(".worldwide-tabs .tabs-title").hide();

  } else{
    jQuery(".active-tab-mob").empty();
    jQuery(".worldwide-tabs .tabs-title").show();
  }
}

function tabClick(){
  jQuery(".worldwide-tabs .btn-tab").click(function(){
    if (jQuery(window).width() < 719) {
      jQuery(".worldwide-tabs .tabs-title").slideDown();
      jQuery(".worldwide-tabs .btn-tab").removeClass('btn-close');
      jQuery(".worldwide-tabs .btn-tab").addClass('btn-open');
    }
  });
  jQuery(".worldwide-tabs .tabs-title li a").click(function(){
    if (jQuery(window).width() < 719) {
      jQuery(".worldwide-tabs .tabs-title").slideUp();
      jQuery(".worldwide-tabs .btn-tab").removeClass('btn-open');
      jQuery(".worldwide-tabs .btn-tab").addClass('btn-close');
      tabPosition();
    }
  });
}


/*draggableImg*/
var draggableImgActive = false;
function draggableImg(){
  if (jQuery(window).width() < 719 && draggableImgActive === false) {
    jQuery(".img-map img").draggable({
      stop: function(event, ui) {
        var helper = ui.helper, pos = ui.position;
        var h = -(helper.outerHeight() - jQuery(helper).parent().outerHeight());
        var w = -(helper.outerWidth() - jQuery(helper).parent().outerWidth());

        if (pos.top >= 0){
          if (pos.left >= 0) {
            helper.animate({top: 0, left: 0 },200);
          } else if (pos.left <= w) {
            helper.animate({top: 0, left: w },200);
          } else{
            helper.animate({ top: 0 },200);
          }
        } else if (pos.top <= h){
          if (pos.left >= 0) {
            helper.animate({top: h, left: 0 },200);
          } else if (pos.left <= w) {
            helper.animate({top: h, left: w },200);
          } else{
            helper.animate({ top: h },200);
          }
        } else{
          if (pos.left >= 0) {
            helper.animate({left: 0 },200);
          } else if (pos.left <= w) {
            helper.animate({left: w },200);
          }
        }
      }
    });
    draggableImgActive = true;
  }else if (jQuery(window).width() >= 720 && draggableImgActive === true){
    jQuery(".img-map img").draggable( "destroy" );
    draggableImgActive = false;
  }
}


jQuery(document).ready(function(){
  jQuery(".js-tabs").tabs();
  tabPosition();
  tabClick();
  draggableImg();
});

jQuery(window).resize(function() {
  tabPosition();
  tabClick();
  jQuery(".img-map img").removeAttr('style');
  draggableImg();
});


(function ($) {
  "use strict";

  $(document).keyup(function (e) {
    if (e.which === 27) {
      // escape : 27
      var popins = $('.popin-avantage');
      if (popins.length > 0) {
        popins.attr('aria-hidden', 'true');
        popins.hide();
      }
    }
  });

  $(document).ready(function () {
    // TWF Business Customer

    var avantageItem = $('.avantage-item');
    avantageItem.focusin(
      function () {
        var targetedItemId = $(this).find('.avantage-item-container').attr('aria-describedby');
        toggleAriaHidden($('#' + targetedItemId), true);
      }
    );

    avantageItem.focusout(
      function () {
        var targetedItemId = $(this).find('.avantage-item-container').attr('aria-describedby');
        toggleAriaHidden($('#' + targetedItemId), false);
      }
    );

    avantageItem.hover(
      function () {
        var targetedItemId = $(this).find('.avantage-item-container').attr('aria-describedby');
        toggleAriaHidden($('#' + targetedItemId), true);
      },
      function () {
        var targetedItemId = $(this).find('.avantage-item-container').attr('aria-describedby');
        toggleAriaHidden($('#' + targetedItemId), false);
      }
    );
  });

  // True if hover in, false if hover out.
  function toggleAriaHidden(buttonHovered, hoverIn) {

    if (hoverIn === true) {
      buttonHovered.attr('aria-hidden', 'false');
      buttonHovered.show();
    }
    else {
      buttonHovered.attr('aria-hidden', 'true');
      buttonHovered.hide();
    }
  }
})(jQuery);

(function ($) {
  "use strict";

  $(document).ready(function () {
    configureFormRGPDDefaultValues();
    configurePopinForm();

    $('.rgpd_open_customized_choices').on('click', function () {
      $('.js-media-popin-rgpd').dialog({
        width: '70%',
        modal: true,
        resizable: false,
        dialogClass: "modal-to-top",
        open: function (event) {
          $('body').css({overflow: "hidden"});
          var overlay = $('.ui-widget-overlay');
          overlay.click(function () {
            $(event.target).dialog('close');
          });
        },
        close: function () {
          $('body').css({overflow: "auto"});
        }
      });
    });

    $('.rgpd_popin_choices .rgpd_deny_all').on('click', function () {
      setAllChoices(0);
    });

    $('.rgpd_popin_choices .rgpd_allow_all').on('click', function () {
      setAllChoices(1);
    });

    var domain = window.location.hostname;
    if (!$.cookie('authorized_config')) {
      document.cookie = '__unam=; expires=Thu, 01 Jan 1970 00:00:01 GMT; domain=' + domain + '; path=/';
      document.cookie = '_gid=; expires=Thu, 01 Jan 1970 00:00:01 GMT; domain=' + domain + '; path=/';
      document.cookie = '_ga=; expires=Thu, 01 Jan 1970 00:00:01 GMT; domain=' + domain + '; path=/';
      document.cookie = '_gat=; expires=Thu, 01 Jan 1970 00:00:01 GMT; domain=' + domain + '; path=/';
      // Disable HubSpot cookies.
      // See https://knowledge.hubspot.com/articles/kcs_article/reports/what-cookies-does-hubspot-set-in-a-visitor-s-browser.
      document.cookie = '__hs_opt_out=1; expires=Thu, 01 Jan 2100 00:00:01 GMT; domain=' + domain + '; path=/';
      $('.rgpd_banner_alert').show();
    }
    else {
      if ($.cookie('__unam')) {
        // Delete sharethis cookie.
        if (readCookie('authorized_config')['sharethis'] === '0') {
          document.cookie = '__unam=; expires=Thu, 01 Jan 1970 00:00:01 GMT; domain=' + domain + '; path=/';
        }
      }

      if (readCookie('authorized_config')['gtm'] === '0') {
        if ($.cookie('_gid')) {
          // Delete google cookie.
          document.cookie = '_gid=; expires=Thu, 01 Jan 1970 00:00:01 GMT; domain=' + domain + '; path=/';
        }

        if ($.cookie('_ga')) {
          // Delete google cookie.
          document.cookie = '_ga=; expires=Thu, 01 Jan 1970 00:00:01 GMT; domain=' + domain + '; path=/';
        }

        if ($.cookie('_gat')) {
          // Delete google cookie.
          document.cookie = '_gat=; expires=Thu, 01 Jan 1970 00:00:01 GMT; domain=' + domain + '; path=/';
        }
      }

    }

    $('.rgpd_accept_everything').on('click', function () {
      // Accept all RGPD choices in the GDPR Banner and save the cookie.
      setAllChoices(1);
      $('.rgpd_popin_choices form input:submit').click();
    });

    // Remove all on click tealium if tealium not allowed.
    var cookieRGPD = readCookie();
    if (cookieRGPD['tealium'] != "1") {
      $('.js-tealium-click').removeAttr('onclick');
    }

    /**
     * Reads the cookie of a previous submission.
     *
     * @param {string} name
     *   Cookie name.
     *
     * @return {array} analyzed_cookie
     */
    function readCookie(name) {
      name = name || "authorized_config";
      var authorized_config = getCookie(name);
      var exploded_config = authorized_config.split('!');
      var analyzed_cookie = [];
      for (var i = 0; i < exploded_config.length; i++) {
        var complete_value = exploded_config[i];
        analyzed_cookie[complete_value.split('=')[0]] = complete_value.split('=')[1];
      }
      return analyzed_cookie;
    }

    function getCookie(cname) {
      var name = cname + "=";
      var decodedCookie = decodeURIComponent(document.cookie);
      var ca = decodedCookie.split(';');
      for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') {
          c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
          return c.substring(name.length, c.length);
        }
      }
      return "";
    }

    // Set previous values from cookies in "configure gdpr" form.
    function configureFormRGPDDefaultValues() {

      var cookieRGPDuser = readCookie("authorized_config");
      if (Object.keys(cookieRGPDuser).length > 0) {

        // Add all rgpd content type authorized by the user.
        var rgpdAuthorizations = [];
        rgpdAuthorizations.push('youtube', 'damdy', 'sharethis', 'getquote', 'mdynamics', 'tealium', 'gtm', 'xiti');

        for (var i = 0; i < rgpdAuthorizations.length; i++) {
          var rgpdName = rgpdAuthorizations[i];
          if (rgpdName in cookieRGPDuser) {
            var rgpdValue = cookieRGPDuser[rgpdName];
            $('form input:radio[name=' + rgpdName + '_rgpd][value=' + rgpdValue + ']').attr('checked', true);
          }
        }
      }

    }

    function configurePopinForm() {
      // Before GDPR user form submission, checks that all questions are answered.
      $('.rgpd_popin_choices .form-submit').on('click', function (e) {

        // Get the number of inputs in the form.
        var nbInputs = $('.rgpd_popin_choices form input:radio[value="0"]').length;
        // Get the number of checked inputs.
        var nbInputsChecked = $('form .form-radios.rgpd_choice_user_form input:radio:checked').length;

        if (nbInputsChecked < nbInputs) {
          // Prevent form submission
          e.preventDefault();

          // Display error validation message.
          $('.rgpd_popin_choices .rgpd_validate_message').fadeIn();
          setTimeout(function () {
            $('.rgpd_popin_choices .rgpd_validate_message').fadeOut();
          }, 3500);
        }

      });
    }

    /*
     * Set all GDPR choices to deny or accept. Deny by default.
     */
    function setAllChoices(acceptAll) {

      if (typeof acceptAll === 'undefined') {
        acceptAll = 1;
      }

      $('.rgpd_popin_choices form input:radio[value="' + acceptAll + '"]').prop('checked', true);
    }

  });
})(jQuery);

function removeParam(key, sourceURL) {
  var rtn = sourceURL.split("?")[0];
  var param;
  var params_arr = [];
  var queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";

  if (queryString !== "") {
    params_arr = queryString.split("&");
    for (var i = params_arr.length - 1; i >= 0; i -= 1) {
      param = params_arr[i].split("=")[0];
      if (param === key) {
        params_arr.splice(i, 1);
      }
    }
    rtn = rtn + "?" + params_arr.join("&");
  }
  return rtn;
}

(function ($) {
  "use strict";

  $(function () {
    // Add class stylized_select to select in form assembly.
    $('.inputWrapper select').parent().addClass('stylized_select');

    var removeDuplicateClasses = function ($ele, className) {
      if ($ele instanceof jQuery) {
        var $duplications = $ele.children('div.' + className);

        $duplications.each(function () {
          $(this).removeClass(className);
        });
      }
    };

    var $stylizedSelects = $('.stylized_select');

    $stylizedSelects.formAlteration();
    removeDuplicateClasses($stylizedSelects, 'stylized_select');

    // For AJAX View form, the submit action will reset pagination.
    $(document).ajaxSend(function (event, request, settings) {
      if (
          typeof settings.extraData !== 'undefined' && typeof settings.extraData.view_name !== 'undefined' &&
          typeof settings.url !== 'undefined' && settings.extraData.view_name === 'list_of_news'
      ) {
        settings.url = removeParam('page', settings.url);
      }
    });

    // For AJAX form, the submit action will update the form content.
    // Refresh stylized select dynamics actions.
    $(document).ajaxSuccess(function (event, request, settings) {
      var url = settings.url;
      var contain = 'views/ajax';
      // AJAX url was for a view form.
      if (url.indexOf(contain) > -1) {
        // selection may be broken, refresh it
        $stylizedSelects = $('.stylized_select');
        // Rebuild stylized label for select.
        $stylizedSelects.formAlteration();
        removeDuplicateClasses($stylizedSelects, 'stylized_select');
      }
    });
  });

  if ($('#edit-field-you-are').length >= 1) {
    $('input[name=field_you_are]').click(
        function () {
          if ($(this).val() === 'totalemployee') {
            $('.brandcenter-form-register-agence').hide();
            $('.brandcenter-form-register-contact').hide();
          }
          else {
            $('.brandcenter-form-register-agence').show();
            $('.brandcenter-form-register-contact').show();
          }
        }
    );
  }

})(jQuery);

function createAgendaSlider() {
  var slideWidth = 276;
  var slideMargin = 14;
  var winWith = jQuery(window).width();
  var len = jQuery('#slider2 li').length;
  var withPager = len > 3 || (winWith < 720 && len > 1);

  if (winWith < 960) {
    if (len <= 3) {
      slideWidth = 225;
      slideMargin = 18;
    } else {
      slideWidth = 190;
      slideMargin = 12;
    }
  }
  else if (len <= 3) {
    slideWidth = 300;
    slideMargin = 24;
  }

  jQuery('#slider2').bxSlider({
    minSlides: 1,
    maxSlides: 3,
    slideWidth: slideWidth,
    slideMargin: slideMargin,
    moveSlides: 1,
    controls: withPager,
    pager: withPager
  });
}

jQuery(document).on("ready", function() {
  createAgendaSlider();
});




(function ($) {
  "use strict";

  $(function () {
    /*************function toggle******************/
    $(".menu-toggable h4:not(.fixed)").click(function () {
      $(this).next("ul").slideToggle("false")
        .siblings("ul:visible").slideUp("false");
      $(this).toggleClass("active");
      $(this).siblings("h4").removeClass("active");
    });
    /*************End function toggle******************/
  });
})(jQuery);



jQuery(document).ready(function () {
  jQuery('.js-lang-content').hide();
  jQuery('.js-btn-lang').unbind('click');

  jQuery('.js-btn-lang').click(function () {
    if (jQuery('.select-lang').hasClass('menu-lang__open')) {
      jQuery('.js-lang-content').slideUp("normal", function () {
        jQuery('.select-lang').removeClass('menu-lang__open');
      });
    }
    else {
      jQuery('.js-lang-content').slideDown("normal");
      jQuery('.select-lang').addClass('menu-lang__open');
    }
  });

  jQuery('.js-list-header-top').click(function () {

    var ownChoicesDropdown = jQuery(this).find('.header-top-menu__multiple-choices');
    // First, close all others dropdown of header-top menu.

    jQuery('.header-top-menu__multiple-choices').not(ownChoicesDropdown).removeClass('visible');

    if (ownChoicesDropdown.hasClass('visible')) {
      ownChoicesDropdown.removeClass('visible');
    }
    else {
      ownChoicesDropdown.addClass('visible');
    }
  });

  jQuery(document).click(function (e) {
    if (!jQuery('.js-list-header-top').is(e.target)) {
      jQuery('.header-top-menu__multiple-choices').removeClass('visible');
    }
  });

});

/**
 * Override for IE.
 */
jQuery(document).ready(function () {
  // To support :last-child in the megagabarit under IE8.
  jQuery(".lt-ie9 .mega-gabarit:last-child .one-columns").css('margin-right', 0);
  jQuery(".lt-ie9 .mega-gabarit:not(:last-child) .two-columns").css('margin-right', "30px");
  // To support :nth-child in the website block.
  jQuery(".lt-ie9 .total-website-item:nth-child(3n+3) .item-node-website").css('margin-right', 0);
  // To support :nth-child in the rte table.
  jQuery(".text-rte table tr:nth-child(2n)").css('background-color', "#F1F1F1");
});

(function($){
    'use strict';

    /* Plugin variables */
    var pluginName, defaultOptions = {};

    /**
     * Constructor.
     */
    function Plugin(element, options) {
        // Merge specific and default options.
        this.options = $.extend({}, defaultOptions, options);

        // Initialize the main element.
        this.$element = (element instanceof $)? element: $(element);

        // Save the instance reference into the DOM element.
        this.$element.data(pluginName, this);

        // Object initialisation.
        if (typeof this.setup === 'function') {
            this.setup();
        }
        if (typeof this.bind === 'function') {
            this.bind();
        }
        if (typeof this.init === 'function') {
            this.init();
        }
    }

    /********** Start plugin specific code **********/

    /* Plugin name. */
    pluginName = 'wrapBxSlider';

    /* Plugin default options. */
    defaultOptions = {
        'itemsClass': '.js-block-slider__items',
        'subItemsElement': 'li',
        'navClass': '.bx-btn',
        'defaultWidth': 1170,
        'breakingPoints': {
            'high': 1170,
            'middle': 720,
            'low': 480,
            'lowest': 320
        },
        'auto': false,
        'pause': 5000,
        'bxControls': false,
        'bxMoveSlides': 1
    };

    /**
     * Setup plugin.
     * e.g. Get DOM elements, setup data...
     */
    Plugin.prototype.setup = function() {
        this.setupGeneric();
        this.$items = this.$element.find(this.options.itemsClass);
        this.$slider = this.$items;
        this.$navItems = this.$element.find(this.options.navClass);
        this.sliderWidth = this.options.defaultWidth;
        this.oldSliderWidth = this.sliderWidth;
        this.pager = this.$items.find(this.options.subItemsElement + ', a').length > this.options.bxMoveSlides;
        this.autoScroll = this.options.auto;
        this.settingsSlider = {};

        var time;
        if ($('body').attr('slider-time').length !== 0) {
          time = $('body').attr('slider-time');
        }
       
        this.options.pause = time;

        if (this.$slider.parents('.mega-gabarit').hasClass('auto-slider')) {
          this.autoScroll = true;
        }
        this.$slider.find(this.options.subItemsElement).each(function (index) {
          $(this).attr('role', 'tabpanel').attr('id', 'panel-' + index);
        });

    };

    /**
     * Bind events.
     */
    Plugin.prototype.bind = function() {
        $(window).on('resize.' + pluginName, this.refreshSlider.bind(this));
        this.$slider.find("li.item a").on("focus", this.refreshSliderTab.bind(this));
    };

    /**
     * Init plugin.
     */
    Plugin.prototype.init = function() {
        this.build();
        this.navigation();
    };

    /**
     * Build bxSlider.
     */
    Plugin.prototype.build = function () {
      this.preBuild();
      var slider = this.$slider.parents('.js-block-slider');
      this.settingsSlider = {
        slideWidth: this.sliderWidth,
        moveSlides: this.options.bxMoveSlides,
        pager: this.pager,
        controls: this.options.bxControls,
        auto: this.autoScroll,
        autoControls: this.autoScroll,
        pause: this.options.pause,
        onSliderLoad: function (currentIndex) {
          $(".js-block-slider__items .bx-clone a").each(function () {
            $(this).attr("tabindex", "-1");
          });
          populatePagerAttr(slider);

        },
        onSlideBefore: function (slider, oldIndex, newIndex) {
          pauseVideoPlayers();
        },
        onSlideAfter: function (slide, oldIndex, newIndex) {
          if ($(slide).hasClass('scald-youtube')) {
            playYoutube(slide);
          }
          else if ($(slide).hasClass('damdy_player')) {
            playDamdy(slide);
          }
          populatePagerAttr(slider);
        }
      };
      this.$slider = this.$items.bxSlider(this.settingsSlider);
      addYoutubeParams();
    };

    function populatePagerAttr(slider) {
      slider.find('.bx-pager').attr('role', 'tablist');
      slider.find('.bx-pager .bx-pager-item').each(function (index) {
        $(this).attr('role', 'tab').attr('aria-controls', 'panel-' + index);
        $(this).find('a').attr('alt', 'Panel ' + index);
      });
      
      var selector = 'li:not(.bx-clone)';

      slider.find(selector).each(function (index) {
        if ($(this).attr('aria-hidden') === 'false') {
          slider.find('.bx-pager [aria-controls="panel-' + index + '"]').attr('tabindex', 0).attr('aria-selected', true);
        }
        else {
          slider.find('.bx-pager [aria-controls="panel-' + index + '"]').attr('tabindex', -1).attr('aria-selected', false);
        }
      });
    }
  
    /**
     * Enable jsapi and disable realtive videos for youtube player.
     */
    function addYoutubeParams() {
      $("ul.block-slider-content__items iframe.scald-youtube").each(function () {
        var url = $(this).attr("src");
        if (url.indexOf("youtu") !== -1 && url.indexOf("enablejsapi=1") === -1) {
          var firstChar = url.indexOf("?") !== -1 ? "&" : "?";
          $(this).attr("src", url + firstChar + "rel=0&enablejsapi=1");
        }
      });
    }

    /**
     * Play youtube player of the slide.
     */
    function playYoutube(slide) {
      slide[0].contentWindow.postMessage('{"event":"command","func":"playVideo","args":""}', '*');
    }

    /**
     * Play damdy player of the slide.
     */
    function playDamdy(slide) {

      if ($(slide).find('iframe').contents().find('video')[0]) {
        $(slide).find('iframe').contents().find('video')[0].play();
      }
    }

    /**
     * Pause all the videos.
     */
    function pauseVideoPlayers() {
      $("ul.block-slider-content__items iframe.scald-youtube").each(function () {
        $(this)[0].contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
      });
      $("ul.block-slider-content__items .damdy-player iframe").contents().find('video').each(function () {
        this.pause();
      });
    }

    /**
     * Navigation event.
     */
    Plugin.prototype.navigation = function() {
        var slider = this.$slider;
        this.$navItems.click(function() {
            if ($(this).hasClass('btn-prev')) {
                slider.goToPrevSlide();
            }
            else {
                slider.goToNextSlide();
            }
        });
    };

    /**
     * Customize slider config before build it.
     */
    Plugin.prototype.preBuild = function() {
        this.updateSliderWidth();
    };

    /**
     * Set the global variables for the plugin.
     * Used as a public method to update the variables.
     */
    Plugin.prototype.setupGeneric = function() {
        this.$size = $(window).width();
    };

    /**
     * Update slider dynamic values (for example, the width regardless of the size of the page),
     * and reload the slider.
     */
    Plugin.prototype.refreshSlider = function() {
        this.setupGeneric();
        this.preBuild();
        // Avoid to reload the slider for no reason.
        if (this.oldSliderWidth !== this.sliderWidth) {
            this.oldSliderWidth = this.sliderWidth;
            this.$slider.reloadSlider({
                slideWidth: this.sliderWidth,
                controls: this.options.bxControls
            });
        }
    };

    /**
     * Update slider dynamic values (for example, the width regardless of the size of the page),
     * and reload the slider.
     */
    Plugin.prototype.refreshSliderTab = function(event) {
      // Get slider index
      var sliderIndex = this.$slider.getCurrentSlide();
      // Get the slide index of the element of the focused element, minus 1 as indexes are 0 based
      var focusedElementSlideIndex = $(event.target).closest("li:not(.bx-clone)").index() - 1;
      var isAuto = false;
      if (this.$slider.parents('.js-block-slider').find(".bx-controls .bx-controls-auto .bx-start").hasClass('active') === true) {
        isAuto = true;
      }
      // If the focused element does not match the slide index the slider thinks it's on then move it manually
      if (sliderIndex !== focusedElementSlideIndex) {
        this.settingsSlider.startSlide = focusedElementSlideIndex;
        this.settingsSlider.autoStart = isAuto;
        this.$slider.reloadSlider(this.settingsSlider);
        event.target.focus();
      }
    };

    /**
     * Update the slider width regardless to the window width.
     */
    Plugin.prototype.updateSliderWidth = function() {
        this.sliderWidth = this.options.defaultWidth;
        if (this.$size < this.options.breakingPoints.middle) {
            this.sliderWidth = this.options.breakingPoints.low;
        }
        else if (this.$size < this.options.breakingPoints.low) {
            this.sliderWidth = this.options.breakingPoints.lowest;
        }
    };

    /********** End plugin specific code **********/

    /* Expose jQuery plugin. */
    $.fn[pluginName] = function(options) {
        return this.each(function() {
            var $this = $(this);
            if (!$this.data(pluginName)) {
                new Plugin($this, options);
            }
        });
    };

})(jQuery);