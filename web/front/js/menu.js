$(function () {
    $(window).load(function () {
        var windowWidth = $(window).width();
        var $mainMenu = $('.main-menu');
        var $menuBtnOpen = $('.js-btn-open');
        var $menuBtnClose = $('.js-btn-close');
        var $blockSharing = $('.block-sharing-search');
        var $menuContent = $('.js-menu-content');
        var $hoverFlying = $('.hover-flying');
        var $hoverFlyingActive = $('.hover-flying-active');
        var $activeMainLink = $('.js-hover-flying-active');
        var menuOpenClass = 'menu-mob__open';
        var searchOpenClass = 'sharing-search__open';
        var activeClass = 'on';
        var hoverFlyingDuration = 300;
        var defaultHoverFlyingPosition = 0;
        var defaultHoverFlyingWidth = 0;
        var defaultHoverFlyingOpacity = 0;

        // Hover inner mega menu
        $('.mega-menu-sub').each(function () {
            var $defaultItem = $('.mega-menu-sub-item.js-default-active', this);
            $('.mega-menu-sub-item', this).hover(function () {
                $defaultItem.removeClass(activeClass);
                $(this).addClass(activeClass);
            }, function () {
                $(this).removeClass(activeClass);
                $defaultItem.addClass(activeClass);
                $(".diagonal-helper").removeAttr("style");
            });
        });
        $(".diagonal-helper").hover(function () {
            $(this).hide(1500);
        });
        $('.mega-menu-item').hover(function () {}, function () {
            $(".diagonal-helper").removeAttr("style");
        });

        // Menu mobile
        $menuBtnOpen.on('click tap taphold', function (e) {
            $(this).hide();
            $blockSharing.addClass(searchOpenClass);
            $menuBtnClose.show();
            $menuContent.slideDown();
            $mainMenu.addClass(menuOpenClass);
            e.preventDefault();
        });

        $menuBtnClose.on('click tap taphold', function (e) {
            $(this).hide();
            $menuBtnOpen.show();
            $mainMenu.removeClass(menuOpenClass);
            $blockSharing.removeClass(searchOpenClass);
            $menuContent.slideUp();
            e.preventDefault();
            return false;
        });

        /* Menu mobile */
        $(window).resize(function () {
            var newWindowWidth = jQuery(window).width();
            if (getScreenSizeMenu(newWindowWidth) !== getScreenSizeMenu(windowWidth)) {
                if ($(window).width() < 959) {
                    $menuBtnClose.hide();
                    $menuBtnOpen.show();
                    $menuContent.slideUp('normal', function () {
                        $mainMenu.removeClass(menuOpenClass);
                    });
                }
                else {
                    $menuContent.show();
                }
            }
            windowWidth = $(window).width();
        });

        function getScreenSizeMenu(resolution) {
            var result = 1;
            if (resolution >= 959) {
                result = 2;
            }
            return result;
        }

        // Hover flying
        setDefaultValuesHoverFlying();
        $('.js-hover-flying, .js-hover-flying-active').mouseenter(function () {
            setPositionHoverFlying($(this));
            if ($(this).find('.mega-menu-sub').length) {
                $('body').addClass('layered');
                $('.menu-overlay').show();
            }
        });
        $('.js-hover-flying, .js-hover-flying-active').mouseleave(function () {
            $('body').removeClass('layered');
            $('.menu-overlay').hide();
        });

        $menuContent.mouseleave(function () {
            $hoverFlying.stop().animate({
                opacity: defaultHoverFlyingOpacity,
                left: defaultHoverFlyingPosition,
                width: defaultHoverFlyingWidth
            }, hoverFlyingDuration);
        });

        /* Hover Flying functions */
        function setDefaultValuesHoverFlying() {
            if ($activeMainLink.length) {
                defaultHoverFlyingPosition = 0;
                defaultHoverFlyingWidth = 0;
                defaultHoverFlyingOpacity = 0;
                setPositionActiveHoverFlying($activeMainLink);
            }
        }

        function setPositionHoverFlying($link) {
            var values = calculatePositionHoverFlying($link);
            $hoverFlying.animate({
                opacity: 1,
                left: values.position,
                width: (values.width - 20)
            }, hoverFlyingDuration);
        }

        function setPositionActiveHoverFlying($link) {
            var values = calculatePositionHoverFlying($link);
            // Avoid weird apparition of normal border bar.
            $hoverFlying.animate({
                opacity: 0,
                left: values.position,
                width: (values.width - 20)
            }, hoverFlyingDuration);

            $hoverFlyingActive.animate({
                opacity: 1,
                left: values.position,
                width: (values.width - 20)
            }, hoverFlyingDuration);
        }

        var navbar = jQuery('.sliding-menu');
        var navbarHeight = navbar.offset()['top'];
        var sloganHeight = 37;
        var mobileHeaderBreakpoint = 960;
        var header = jQuery('header');

        function setStickyMenu() {
            var topOffset = jQuery(window).scrollTop();
            var windowWidth = jQuery(window).width();
            var activeMenu = jQuery('.js-hover-flying-active');
            if ((topOffset + sloganHeight >= navbarHeight || windowWidth < mobileHeaderBreakpoint) && !navbar.hasClass('sticky-menu')) {
                navbar.addClass('sticky-menu');
                if (activeMenu.length > 0) {
                    jQuery('.hover-flying-active').animate({
                        left: activeMenu.position().left + 100
                    }, 300);
                }
            }
            else if ((topOffset + sloganHeight < navbarHeight || topOffset === 0) && navbar.hasClass('sticky-menu') && windowWidth >= mobileHeaderBreakpoint) {
                navbar.removeClass('sticky-menu');
                if (activeMenu.length > 0) {
                    jQuery('.hover-flying-active').animate({
                        left: activeMenu.position().left
                    }, 300);
                }
            }
            if (!navbar.hasClass('sticky-menu')) {
                header.height(header.height());
            }
        }

        setStickyMenu();

        jQuery(window).scroll(setStickyMenu);

        function calculatePositionHoverFlying($link) {
            var width = $link.innerWidth();
            var position = $link.position().left;

            return {
                width: width,
                position: position
            };
        }
    });
});
