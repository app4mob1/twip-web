-- phpMyAdmin SQL Dump
-- version 4.7.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 17, 2019 at 11:54 AM
-- Server version: 5.6.35
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `twip`
--

-- --------------------------------------------------------

--
-- Table structure for table `coupon`
--

CREATE TABLE `coupon` (
  `id` int(11) NOT NULL,
  `valeur` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `coupon`
--

INSERT INTO `coupon` (`id`, `valeur`, `code`) VALUES
(2, '16', 14674);

-- --------------------------------------------------------

--
-- Table structure for table `fos_user`
--

CREATE TABLE `fos_user` (
  `id` int(11) NOT NULL,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `admin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nom_societe` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `societe_id` int(11) DEFAULT NULL,
  `domain` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telephone` int(11) DEFAULT NULL,
  `sms` tinyint(1) NOT NULL DEFAULT '0',
  `mail` tinyint(1) DEFAULT NULL,
  `adresse` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fos_user`
--

INSERT INTO `fos_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`, `admin`, `nom_societe`, `societe_id`, `domain`, `telephone`, `sms`, `mail`, `adresse`, `nom`) VALUES
(4, 'Admin', 'admin', 'admin@gmail.com', 'admin@gmail.com', 1, NULL, '$2y$13$1i7GZjZZ6D1FqKuAfXV6H.HfSmwPy76OYdiMFi2XplwW9pKzg2wj6', '2019-11-11 12:26:18', NULL, NULL, 'a:1:{i:0;s:10:\"ROLE_ADMIN\";}', NULL, NULL, NULL, NULL, NULL, 0, 0, '', ''),
(5, 'gerant01', 'gerant01', 'gerant@gmail.com', 'gerant@gmail.com', 1, NULL, '$2y$13$JhuEQDxvdTUkI5sRJn4nteAhuXxUd06jaCjCFcNaS6BFWRFQFRZ/C', '2019-11-08 15:47:15', NULL, NULL, 'a:1:{i:0;s:11:\"ROLE_GERANT\";}', NULL, NULL, NULL, NULL, NULL, 0, 0, '', ''),
(34, 'gerant02', 'gerant02', 'gerant02@gmail.com', 'gerant02@gmail.com', 0, NULL, '$2y$13$dGPfFWNAvfyv9Kl.BB/x/OfdIEO.jbHJ8KppJvhM5ooGU3gz5EKb6', NULL, 'M1AAz0qqcMdIc13ouV0XZ0xM44rpUp0OBUSgsS5pLlE', NULL, 'a:1:{i:0;s:11:\"ROLE_GERANT\";}', NULL, NULL, NULL, NULL, NULL, 0, 0, '', ''),
(35, 'gerant03', 'gerant03', 'gerant03@gmail.com', 'gerant03@gmail.com', 0, NULL, '$2y$13$Ic9GChKOpilhLjmbfnYrT.rzlzMi34QiCgVdcVcS.rQB3goreZilW', NULL, 'VU3-tL4UH9FsqB_eIh3Jfe1AjcuodK-iH_G5GedO694', NULL, 'a:1:{i:0;s:11:\"ROLE_GERANT\";}', NULL, NULL, NULL, NULL, NULL, 0, 0, '', ''),
(48, 'employe3', 'employe3', 'epmpkkk@gmail.com', 'epmpkkk@gmail.com', 1, NULL, '$2y$13$lAThHanObimNAlc7CIEngO7BWCi6JALSepjA5A68H4KMJsPP6wjW2', '2019-11-04 09:33:52', NULL, NULL, 'a:1:{i:0;s:12:\"ROLE_EMPLOYE\";}', 'gerant01', NULL, NULL, NULL, NULL, 0, 0, '', ''),
(49, 'employe4', 'employe4', 'employ4@gmail.com', 'employ4@gmail.com', 0, NULL, '$2y$13$1ztmLTmBOdWUPclou.Kx8ur42Xk4YDApVRTd03Z5f4IRgbFpfkVZK', NULL, '6hXnCJkqqqvulLp_J0JQQ0kVzaym7ZHbW23ycT1igpw', NULL, 'a:1:{i:0;s:12:\"ROLE_EMPLOYE\";}', NULL, NULL, NULL, NULL, NULL, 0, 0, '', ''),
(59, 'houssaien11', 'houssaien11', 'client11@gmail.com', 'client11@gmail.com', 0, NULL, '$2y$13$2n.arzUITNhTRPP1yb4Z1esw9Mf5dPIGs8yp02.bK3On73tO408Ki', NULL, 'UHYQdozeirTKwfBMuC0x-ZDIdQCNAZ7prYiM5hnjiH8', NULL, 'a:1:{i:0;s:11:\"ROLE_CLIENT\";}', NULL, NULL, 12, NULL, NULL, 0, 0, '', ''),
(60, 'client05', 'client05', 'client05@gmail.com', 'client05@gmail.com', 1, NULL, '$2y$13$DDrOGiFHLsavj.enYboNE.b.ds/f5Dt76.qjw1OVfz1mPGR1pz3YO', NULL, 'teD60o60v4SOLjaQ84gL4_fIgbUaJYfAwHMRccMVf_4', NULL, 'a:1:{i:0;s:11:\"ROLE_CLIENT\";}', NULL, NULL, 14, NULL, NULL, 0, 0, '', ''),
(61, 'client011', 'client011', 'client011@gmail.com', 'client011@gmail.com', 0, NULL, '$2y$13$KeubJpkvbdZLgCg3Spxgx.RqBxdKq31CF7hAJEXmEN88xNpf1c/4W', NULL, 'YAhDlueVTQ2EPmIwzJCgXlFEDKOXNxmrVqNTlMuhiO4', NULL, 'a:1:{i:0;s:11:\"ROLE_CLIENT\";}', NULL, NULL, 14, NULL, NULL, 0, 0, '', ''),
(63, 'houssaien92', 'houssaien92', 'benselemhoussaien@gmail.com', 'benselemhoussaien@gmail.com', 1, NULL, '$2y$13$ppfJU4YUzrsfLlRjuFz/7O4FJEi5JkEwfMKbrJis.xVhrWe03Y/ge', '2019-11-08 15:44:04', NULL, NULL, 'a:1:{i:0;s:11:\"ROLE_CLIENT\";}', NULL, NULL, 11, NULL, NULL, 0, 0, '', ''),
(64, 'houssaien', 'houssaien', 'essai111@gmail.com', 'essai111@gmail.com', 0, NULL, '$2y$13$iX7gtndbzIC3jrN/vtamb.9gFcwphdUvlmAx5LVS3MQeDjR3RsDgm', NULL, 'ZXxPspjpPSD1sGh8q_QKZbouZ59GEZGy65d7yLbcc9k', NULL, 'a:1:{i:0;s:11:\"ROLE_CLIENT\";}', NULL, NULL, 7, NULL, NULL, 0, 0, '', ''),
(65, 'houssaien22', 'houssaien22', 'houssaien.bensalem@esprit.tn', 'houssaien.bensalem@esprit.tn', 1, NULL, '$2y$13$f.YBMWiyU6ZXofJT3HaOp.e43QCvaqCGhZcigZcz6hhPBZM3.NewG', '2019-11-08 16:39:24', NULL, NULL, 'a:1:{i:0;s:11:\"ROLE_CLIENT\";}', NULL, NULL, 9, NULL, NULL, 0, 0, '', ''),
(66, 'etst123', 'etst123', 'test@gmail.com', 'test@gmail.com', 0, NULL, '$2y$13$1hGw695/VzYSA6wvEyNLiej7WVRpoBuukhXyfwNIruBqzewALpaSK', NULL, '-mXx1HF29PzpB5uBsLq81-mpUHmGedgKsD2-oAvxY-k', NULL, 'a:1:{i:0;s:11:\"ROLE_CLIENT\";}', NULL, 'Renault', 14, NULL, NULL, 0, 0, '', ''),
(67, 'employe123', 'employe123', 'employ123@gmail.com', 'employ123@gmail.com', 1, NULL, '$2y$13$qZyygYXWv0s0PETBRwMJ5uph5BcMyFyDXbeqnSWT7lg4P8QptQLvy', NULL, 'nDjsPKG_B89gHEPTIHhNrjJ9lq6embxvt78ec0PwzRk', NULL, 'a:1:{i:0;s:11:\"ROLE_CLIENT\";}', NULL, 'societe1', 14, NULL, NULL, 0, 0, '', ''),
(68, 'test44', 'test44', 'test4@gamil.com', 'test4@gamil.com', 1, NULL, '$2y$13$/BsjR1lA49XGL3cwTPaht.tchHmSROTeCuNuys0KZJ0XqxoPDmCD2', '2019-11-08 10:59:28', NULL, NULL, 'a:1:{i:0;s:11:\"ROLE_CLIENT\";}', NULL, 'total', 13, NULL, NULL, 0, 0, '', ''),
(69, 'test007', 'test007', 'test007@test.com', 'test007@test.com', 1, NULL, '$2y$13$MKvB/xlixhUCHyvPlwfjselb5y7ckIJQl4vhGFTkniCQA679nfzWS', '2019-11-08 13:50:56', NULL, NULL, 'a:1:{i:0;s:11:\"ROLE_CLIENT\";}', NULL, NULL, 11, '', NULL, 0, 0, '', ''),
(76, 'newclient', 'newclient', 'newclient@gmail.com', 'newclient@gmail.com', 0, NULL, '$2y$13$npUJAL0X5EsUo9Oet27Fg.pEXbj1D0tE2JtnlXvywSkjaccrJcob2', NULL, 'Zzrqw54RAftybpUJrwlq0tLYKz6rvzMmxUwZC8pFuGk', NULL, 'a:1:{i:0;s:11:\"ROLE_CLIENT\";}', NULL, NULL, 9, NULL, NULL, 0, 0, '', ''),
(89, 'adnen.chouibi@gmail.com', 'adnen.chouibi@gmail.com', 'adnen.chouibi@gmail.com', 'adnen.chouibi@gmail.com', 1, NULL, '$2y$13$TJPqyJ4FxvN0uCizGnGwT.Jio5o7chx7IMsUgG91c7IjbX9GF4cwK', '2019-11-09 22:25:57', NULL, NULL, 'a:1:{i:0;s:11:\"ROLE_CLIENT\";}', NULL, NULL, NULL, NULL, NULL, 0, 0, '', ''),
(90, 'adnen.chouibi@app4mob.net', 'adnen.chouibi@app4mob.net', 'adnen.chouibi@app4mob.net', 'adnen.chouibi@app4mob.net', 1, NULL, '$2y$13$GwqDQwf.a3iKvmuafvms9uHfEzj2y5840rIHAB52U4ryDoiWi5rN6', '2019-11-09 23:46:46', NULL, NULL, 'a:1:{i:0;s:11:\"ROLE_CLIENT\";}', NULL, 'app4mobbbbb', 12, NULL, 28226206, 1, 0, 'Cité Naser, Tunis', 'Adnen chouibi'),
(91, 'adnen@app4mob.net', 'adnen@app4mob.net', 'adnen@app4mob.net', 'adnen@app4mob.net', 1, NULL, '$2y$13$bFEjllUv4E2GdOn8cTh03OcUtMTjECIF35a9Q4BBCQ90uwkT8lkiK', '2019-11-16 20:53:21', NULL, NULL, 'a:1:{i:0;s:11:\"ROLE_CLIENT\";}', NULL, NULL, 12, NULL, 28226206, 1, 0, 'Cité Naser, Tunis', 'Adnen chouibi');

-- --------------------------------------------------------

--
-- Table structure for table `lavage`
--

CREATE TABLE `lavage` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `miniPrix` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `minitemps` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `citadinePrix` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `citadineTemps` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `monoPrix` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `monoTemps` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `camionPrix` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `camionTemps` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `motoPrix` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `motoTemps` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activation` tinyint(1) NOT NULL,
  `admin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `lavage`
--

INSERT INTO `lavage` (`id`, `name`, `miniPrix`, `minitemps`, `citadinePrix`, `citadineTemps`, `monoPrix`, `monoTemps`, `camionPrix`, `camionTemps`, `motoPrix`, `motoTemps`, `activation`, `admin`) VALUES
(3, 'Lavage complet intérieur extérieur', '5', '15', '7', '60', '9', '60', '13', '100', '4', '20', 0, NULL),
(4, 'Majoration véhicules très sale', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(5, 'Forfait avant revente ou restitution  véhicule', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(6, 'Efface micro-rayures', '17', '10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(7, 'Pack intérieur protection(tableau de bord, chrome, désinfection climatisation)', '10', '17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(8, 'Détachant tissu', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `participant`
--

CREATE TABLE `participant` (
  `id` int(11) NOT NULL,
  `station` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prenom` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telephone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `sms` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mail_sms` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `produit`
--

CREATE TABLE `produit` (
  `id` int(11) NOT NULL,
  `libelle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `decription` text COLLATE utf8_unicode_ci,
  `prix` double DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `image` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `produit`
--

INSERT INTO `produit` (`id`, `libelle`, `decription`, `prix`, `updated_at`, `image`) VALUES
(2, 'Total Quartz 9000', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has su', 30, '2019-10-18 18:10:28', '0001.jpg'),
(3, 'Brosse poils d\'animaux Total Wash', NULL, 5.5, '0000-00-00 00:00:00', '002.jpg'),
(4, 'Brosse de lavage pour jantes Total Wash', NULL, 5.5, '0000-00-00 00:00:00', '003.jpg'),
(5, 'Gant de lavage double face Total Wash', NULL, 5.5, '0000-00-00 00:00:00', '004.jpg'),
(6, 'Bombe anti-crevaison 500 ml', NULL, 15.5, '0000-00-00 00:00:00', '005.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `reservation`
--

CREATE TABLE `reservation` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `heureResservation` datetime DEFAULT NULL,
  `coupon_id` int(11) DEFAULT NULL,
  `confirmation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lavage` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `voiture` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updateAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `reservation`
--

INSERT INTO `reservation` (`id`, `user_id`, `heureResservation`, `coupon_id`, `confirmation`, `lavage`, `voiture`, `updateAt`) VALUES
(1, 65, '0000-00-00 00:00:00', NULL, 'en attente', '', '', NULL),
(2, 65, NULL, NULL, 'en attente', '', '', NULL),
(3, 65, NULL, NULL, 'en attente', '', '', NULL),
(4, 63, NULL, NULL, 'en attente', '', '', NULL),
(5, 63, NULL, NULL, 'en attente', '', '', NULL),
(6, 63, NULL, NULL, 'en attente', '', '', NULL),
(7, 63, NULL, NULL, 'en attente', '', '', NULL),
(8, 63, NULL, NULL, 'en attente', '3', '', NULL),
(9, 63, NULL, NULL, 'en attente', '4', '', NULL),
(10, 63, NULL, NULL, 'en attente', '3', '', NULL),
(11, 63, NULL, NULL, 'refuse', '3', '', NULL),
(12, 63, NULL, NULL, 'accepte', '4', '', NULL),
(13, 63, NULL, NULL, 'accepte', '4', '', NULL),
(14, 65, NULL, NULL, 'en attente', '3', '169', NULL),
(15, 63, NULL, NULL, 'en attente', '3', '168', '2019-11-08 00:00:00'),
(16, 63, NULL, NULL, 'en attente', '3', '170', '2019-11-08 09:10:10'),
(17, 65, NULL, NULL, 'en attente', '3', '171', '2019-11-08 16:40:46');

-- --------------------------------------------------------

--
-- Table structure for table `societe`
--

CREATE TABLE `societe` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jourDisponible` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:array)',
  `mail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adresse` text COLLATE utf8_unicode_ci,
  `remise` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `societe`
--

INSERT INTO `societe` (`id`, `nom`, `jourDisponible`, `mail`, `adresse`, `remise`) VALUES
(7, 'total5', 'a:3:{i:0;s:5:\"Lundi\";i:1;s:5:\"Jeudi\";i:2;s:8:\"Dimanche\";}', NULL, NULL, '0'),
(8, 'total', 'a:3:{i:0;s:5:\"Lundi\";i:1;s:5:\"Jeudi\";i:2;s:6:\"Samedi\";}', NULL, NULL, '0'),
(9, 'jklutl', 'a:3:{i:0;s:5:\"Lundi\";i:1;s:6:\"Samedi\";i:2;s:8:\"Dimanche\";}', NULL, NULL, '0'),
(11, 'total8', 'a:2:{i:0;s:5:\"Lundi\";i:1;s:6:\"Samedi\";}', NULL, NULL, '0'),
(12, 'app4mob', 'a:2:{i:0;s:1:\"5\";i:1;s:1:\"4\";}', 'app4mob.net', 'a:2:{i:0;s:18:\"Cité Naser, Tunis\";i:1;s:8:\"Vendredi\";}', '0'),
(13, 'total', 'a:2:{i:0;s:5:\"Lundi\";i:1;s:8:\"Vendredi\";}', 'total.com', NULL, '0'),
(14, 'societe1', 'a:3:{i:0;s:5:\"Lundi\";i:1;s:5:\"Jeudi\";i:2;s:8:\"Dimanche\";}', 'societe.tn', NULL, '0');

-- --------------------------------------------------------

--
-- Table structure for table `societe_zone`
--

CREATE TABLE `societe_zone` (
  `societe_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `societe_zone`
--

INSERT INTO `societe_zone` (`societe_id`, `zone_id`) VALUES
(12, 1),
(13, 1),
(13, 2),
(14, 1);

-- --------------------------------------------------------

--
-- Table structure for table `station`
--

CREATE TABLE `station` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_zone`
--

CREATE TABLE `user_zone` (
  `user_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vehicule`
--

CREATE TABLE `vehicule` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `immatricule` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `modele` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `categorie` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updateAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `vehicule`
--

INSERT INTO `vehicule` (`id`, `user_id`, `immatricule`, `modele`, `type`, `categorie`, `updateAt`) VALUES
(6, 90, '172 tun 5777', 'Polo7', '2', '0', '2019-11-10 02:59:06'),
(7, 90, '123987', 'tmax', '1', '1', '2019-11-10 02:59:06'),
(8, 90, 'aaaa', 'aaa', '4', '0', '2019-11-10 02:59:06'),
(9, 91, '198 TUN 3652', 'Golf7', '1', '0', '2019-11-10 16:36:57'),
(10, 91, '157 TUN 96325', 'Infinity', '2', '1', '2019-11-10 16:36:57'),
(11, 91, '125 TUN 6523', 'Volswagen', '2', '0', '2019-11-10 16:36:57'),
(12, 91, '98569 TN 14', 'Z-MAX', '4', '1', '2019-11-10 16:36:57'),
(13, 91, '45 Tun 8542', 'Golf 2 ', '0', '1', '2019-11-10 16:36:57');

-- --------------------------------------------------------

--
-- Table structure for table `zone`
--

CREATE TABLE `zone` (
  `id` int(11) NOT NULL,
  `zone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `zone`
--

INSERT INTO `zone` (`id`, `zone`) VALUES
(1, 'Tunis1'),
(2, 'Tunis2');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `coupon`
--
ALTER TABLE `coupon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fos_user`
--
ALTER TABLE `fos_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479C05FB297` (`confirmation_token`),
  ADD KEY `IDX_957A6479FCF77503` (`societe_id`);

--
-- Indexes for table `lavage`
--
ALTER TABLE `lavage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `participant`
--
ALTER TABLE `participant`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produit`
--
ALTER TABLE `produit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_C454C682A76ED395` (`user_id`),
  ADD KEY `IDX_C454C68266C5951B` (`coupon_id`);

--
-- Indexes for table `societe`
--
ALTER TABLE `societe`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `societe_zone`
--
ALTER TABLE `societe_zone`
  ADD PRIMARY KEY (`societe_id`,`zone_id`),
  ADD KEY `IDX_C2C4FC16FCF77503` (`societe_id`),
  ADD KEY `IDX_C2C4FC169F2C3FAB` (`zone_id`);

--
-- Indexes for table `station`
--
ALTER TABLE `station`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_zone`
--
ALTER TABLE `user_zone`
  ADD PRIMARY KEY (`user_id`,`zone_id`),
  ADD KEY `IDX_DA6A8CCEA76ED395` (`user_id`),
  ADD KEY `IDX_DA6A8CCE9F2C3FAB` (`zone_id`);

--
-- Indexes for table `vehicule`
--
ALTER TABLE `vehicule`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_292FFF1D8D93D649` (`user_id`);

--
-- Indexes for table `zone`
--
ALTER TABLE `zone`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `coupon`
--
ALTER TABLE `coupon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `fos_user`
--
ALTER TABLE `fos_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;
--
-- AUTO_INCREMENT for table `lavage`
--
ALTER TABLE `lavage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `participant`
--
ALTER TABLE `participant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `produit`
--
ALTER TABLE `produit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `reservation`
--
ALTER TABLE `reservation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `societe`
--
ALTER TABLE `societe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `station`
--
ALTER TABLE `station`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vehicule`
--
ALTER TABLE `vehicule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `zone`
--
ALTER TABLE `zone`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `fos_user`
--
ALTER TABLE `fos_user`
  ADD CONSTRAINT `FK_957A6479FCF77503` FOREIGN KEY (`societe_id`) REFERENCES `societe` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `reservation`
--
ALTER TABLE `reservation`
  ADD CONSTRAINT `FK_C454C68266C5951B` FOREIGN KEY (`coupon_id`) REFERENCES `coupon` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_C454C682A76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `societe_zone`
--
ALTER TABLE `societe_zone`
  ADD CONSTRAINT `FK_C2C4FC169F2C3FAB` FOREIGN KEY (`zone_id`) REFERENCES `zone` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_C2C4FC16FCF77503` FOREIGN KEY (`societe_id`) REFERENCES `societe` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_zone`
--
ALTER TABLE `user_zone`
  ADD CONSTRAINT `FK_DA6A8CCE9F2C3FAB` FOREIGN KEY (`zone_id`) REFERENCES `zone` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_DA6A8CCEA76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `vehicule`
--
ALTER TABLE `vehicule`
  ADD CONSTRAINT `FK_292FFF1D8D93D649` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
