<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    
    
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {

          $em = $this->getDoctrine()->getManager();
       
        
            return $this->render('default/home.html.twig');
    

    }
}
