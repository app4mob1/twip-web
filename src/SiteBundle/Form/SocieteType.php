<?php

namespace SiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Validator\Constraints\Choice;

class SocieteType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nom')
            ->add('mail')
            ->add('zone')
            ->add('jourRes', ChoiceType::class, [
                'multiple' => true,
                'expanded' => true,
                'choices' => [
                    'Lundi' => 0,
                    'Mardi' => 1,
                    'Mercredi' => 2,
                    'Jeudi' => 3,
                    'Vendredi' => 4,
                    'Samedi' => 5,
                    'Dimanche' => 6,
                ], 'label' => 'Selectionner jours'
            ])
            ->add('adresse', CollectionType::class, [           
                'entry_options' => [
                    'attr' => ['class' => 'adresse'],
                ], 
            ])
            ->add('remise');
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SiteBundle\Entity\Societe'
        ));
    }
    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'sitebundle_societe';
    }


}
