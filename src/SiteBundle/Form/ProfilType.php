<?php

namespace SiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class ProfilType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nom',null,['required'  => true,'label' => 'Nom & prénom'])
        ->add('immatricule',null,['required'  => true,'label' => 'Immatriculation'])
        ->add('modele',null,['required'  => true,'label' => 'Modéle'])
        ->add('telephone',null,['required'  => true,'label' => 'Téléphone'])
        ->add('type', ChoiceType::class, [
            'multiple'=>false,
            'label_attr' => array('class' => 'radio-inline'),
            'expanded'=>true,
            'choices'  => [
                'Mini' => 'mini',
                'Citadine' => 'citadine',
            ],'label' => ''
        ],null,['required'  => true])
        ->add('sms', CheckboxType::class, [
            'label'    => 'Je souhaite recevoir des SMS de la part de Twip',
            'data' => true,
            'required' => false,
        ])
        ->add('categrieveh', ChoiceType::class, [
            'multiple'=>false,
            'label_attr' => array('class' => 'radio-inline'),
            'expanded'=>true,
            'choices'  => [
                'véhicule société' => 'société',
                'véhicule personnel' =>  'personnel',
                
            ],'label' => ' '
        ],null,['required'  => true]);
          
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SiteBundle\Entity\Vehicule'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'sitebundle_profil';
    }


}
