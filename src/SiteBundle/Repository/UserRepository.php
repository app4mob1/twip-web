<?php

namespace SiteBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * DepenceRepository
 *
 * This class was generated by the PhpStorm "Php Annotations" Plugin. Add your own custom
 * repository methods below.
 * @method getDoctrine()
 */
class UserRepository extends EntityRepository
{


    public function getUsersByRole($role)
    {
        return $this->getEntityManager()
            ->createQuery(
                "SELECT u FROM SiteBundle:User u where u.roles LIKE '%$role%'"
            )
            ->getResult();
    }

}





