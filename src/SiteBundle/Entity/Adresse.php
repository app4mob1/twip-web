<?php
// src/AppBundle/Entity/User.php

namespace SiteBundle\Entity;


namespace SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="adresse")
 */
class Adresse
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id",type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;


    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string",nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(name="societe_id",type="integer", nullable=true)
     */
    public $societe_id;

    /**
     * @ORM\ManyToOne(targetEntity="Societe")
     * @ORM\JoinColumn(name="societe_id",referencedColumnName="id", nullable=true ,onDelete="SET NULL")
     */
    private $societe;


    /**
     *
     * @ORM\Column(name="zone_id",type="integer", nullable=true)
     */
    public $zone_id;

    /**
     * @ORM\ManyToOne(targetEntity="Zone")
     * @ORM\JoinColumn(name="zone_id",referencedColumnName="id", nullable=true ,onDelete="SET NULL")
     */
    private $zone;



    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $is_active = true;

    /**
     * autorisation constructor.
     * @param $id
     */
    public function __construct()
    {

    }

    public function __toString()
    {
        return $this.$this->name;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->is_active;
    }

    /**
     * @param bool $is_active
     */
    public function setIsActive($is_active)
    {
        $this->is_active = $is_active;
    }


    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSocieteId()
    {
        return $this->societe_id;
    }

    /**
     * @param mixed $societe_id
     */
    public function setSocieteId($societe_id)
    {
        $this->societe_id = $societe_id;
    }

    /**
     * @return mixed
     */
    public function getSociete()
    {
        return $this->societe;
    }

    /**
     * @param mixed $societe
     */
    public function setSociete($societe)
    {
        $this->societe = $societe;
    }

    /**
     * @return mixed
     */
    public function getZoneId()
    {
        return $this->zone_id;
    }

    /**
     * @param mixed $zone_id
     */
    public function setZoneId($zone_id)
    {
        $this->zone_id = $zone_id;
    }

    /**
     * @return mixed
     */
    public function getZone()
    {
        return $this->zone;
    }

    /**
     * @param mixed $zone
     */
    public function setZone($zone)
    {
        $this->zone = $zone;
    }


}
