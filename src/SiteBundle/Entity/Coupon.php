<?php
// src/AppBundle/Entity/User.php

namespace SiteBundle\Entity;


namespace SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="coupon")
 */
class Coupon
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id",type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;


    /**
     * @var string
     *
     * @ORM\Column(name="valeur", type="string",nullable=true)
     */
    private $valeur;

     /**
     * @var string
     *
     * @ORM\Column(name="code", type="string",nullable=true)
     */
    private $code;
     /**
     * @var string
     *
     * @ORM\Column(name="is_used", type="boolean")
     */
    private $is_used = 0;

       /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    
     /**           
     * @return float           
     */
    public function getValeur()
    {
        return $this->valeur;
    }

    /**
     * @param float $valeur
     */
    public function setValeur($valeur)
    {
        $this->valeur = $valeur;
    }

     /**           
     * @return string           
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }


    public function __toString() {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getisUsed()
    {
        return $this->is_used;
    }

    /**
     * @param string $is_used
     */
    public function setIsUsed($is_used)
    {
        $this->is_used = $is_used;
    }



}
