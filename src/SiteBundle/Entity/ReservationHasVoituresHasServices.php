<?php
// src/AppBundle/Entity/User.php

namespace SiteBundle\Entity;


namespace SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="reservation_has_voitures_has_services")
 */
class ReservationHasVoituresHasServices
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id",type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @ORM\ManyToOne(targetEntity="ReservationHasVoitures")
     * @ORM\JoinColumn(name="reservation_has_voitures_id",referencedColumnName="id", nullable=true ,onDelete="CASCADE")
     */
    private $reservation_has_voitures;


    /**
     * @ORM\ManyToOne(targetEntity="Lavage")
     * @ORM\JoinColumn(name="lavage_id",referencedColumnName="id", nullable=true ,onDelete="CASCADE")
     */
    private $lavage;

    /**
     * @ORM\Column(name="reservation_has_voitures_id",type="integer")
     */
    public $reservation_has_voitures_id;

    /**
     * @ORM\Column(name="lavage_id",type="integer")
     */
    public $lavage_id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getReservationHasVoitures()
    {
        return $this->reservation_has_voitures;
    }

    /**
     * @param mixed $reservation_has_voitures
     */
    public function setReservationHasVoitures($reservation_has_voitures)
    {
        $this->reservation_has_voitures = $reservation_has_voitures;
    }

    /**
     * @return mixed
     */
    public function getLavage()
    {
        return $this->lavage;
    }

    /**
     * @param mixed $lavage
     */
    public function setLavage($lavage)
    {
        $this->lavage = $lavage;
    }

    /**
     * @return mixed
     */
    public function getReservationHasVoituresId()
    {
        return $this->reservation_has_voitures_id;
    }

    /**
     * @param mixed $reservation_has_voitures_id
     */
    public function setReservationHasVoituresId($reservation_has_voitures_id)
    {
        $this->reservation_has_voitures_id = $reservation_has_voitures_id;
    }

    /**
     * @return mixed
     */
    public function getLavageId()
    {
        return $this->lavage_id;
    }

    /**
     * @param mixed $lavage_id
     */
    public function setLavageId($lavage_id)
    {
        $this->lavage_id = $lavage_id;
    }




}
