<?php
// src/AppBundle/Entity/User.php

namespace SiteBundle\Entity;


namespace SiteBundle\Entity;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContext;

/**
 * @Vich\Uploadable
 * @ORM\Table(name="produit")
 * @ORM\Entity(repositoryClass="SiteBundle\Repository\ParticipantRepository")
 */
class Produit
{

    /**
     * @ORM\Id
     * @ORM\Column(name="id",type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

     /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string",nullable=true)
     */
    private $libelle;

    

    /**
     * @var string
     *
     * @ORM\Column(name="decription", type="string",nullable=true)
     */
    private $decription;



     /**
     * @ORM\Column(name="prix", type="float",nullable=true)
     * @var float
     */
    private $prix=0;
     /**
     * @ORM\Column(type="string", length=500,nullable=true)
     * @var string
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="product_images", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    public $updatedAt;


    

    public function __construct()
    {

        $this->setUpdatedAt(new \DateTime());
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

   
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getImage()
    {
        return $this->image;
    }


    /**
     * @param \DateTime $updatedAt
     * @return Article
     */
    public function setUpdatedAt($updatedAt)
    {

        $this->updatedAt = new \DateTime('now');
        return $this;
    }
    /**
         * Get updatedAt
         *
         * @return \DateTime
         */
        public function getUpdatedAt()
        {
            return $this->updatedAt;
        }

    public function __toString()
    {
        return $this->getDecription();
    }

 

    /**
     * Get the value of decription
     *
     * @return  string
     */ 
    public function getDecription()
    {
        return $this->decription;
    }

    /**
     * Set the value of decription
     *
     * @param  string  $decription
  
     */ 
    public function setDecription($decription)
    {
        $this->decription = $decription;

        return $this;
    }

    /**
     * Get the value of libelle
     *
     * @return  string
     */ 
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set the value of libelle
     *
     * @param  string  $libelle
     */ 
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get the value of prix
     *
     * @return  float
     */ 
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set the value of prix
     *
     * @param  float  $prix
     */ 
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }
}
