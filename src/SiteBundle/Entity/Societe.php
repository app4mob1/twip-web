<?php
// src/AppBundle/Entity/User.php

namespace SiteBundle\Entity;


namespace SiteBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="SiteBundle\Repository\SocieteRepository")
 * @ORM\Table(name="societe")
 */
class Societe
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id",type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string")
     */
    private $nom;


    /**
     * @var string
     *
     * @ORM\Column(name="mail", type="string",nullable=true)
     */
    private $mail;

    /**
     * @var float
     *
     * @ORM\Column(name="remise", type="float",nullable=true)
     */
    private $remise;


    /**
     * @ORM\OneToMany(targetEntity="Adresse", mappedBy="societe",   cascade={"persist"})
     */
    private $adresses;


    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $is_active = true;

    /**
     * autorisation constructor.
     * @param $id
     */
    public function __construct()
    {
        $this->adresses = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return $this->nom;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->is_active;
    }

    /**
     * @param bool $is_active
     */
    public function setIsActive($is_active)
    {
        $this->is_active = $is_active;
    }




    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return string
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * @param string $mail
     */
    public function setMail($mail)
    {
        $this->mail = $mail;
    }

    /**
     * @return float
     */
    public function getRemise()
    {
        return $this->remise;
    }

    /**
     * @param float $remise
     */
    public function setRemise($remise)
    {
        $this->remise = $remise;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAdresses()
    {
        return $this->adresses;
    }

    /**
     * @param mixed $adresses
     */
    public function setAdresses($adresses)
    {
        $this->adresses = $adresses;
    }


    /**
     * Add priceList
     *
     */
    public function addAdresses(Adresse $adresse)
    {
        $this->adresses[] = $adresse;

        return $this;
    }


}
