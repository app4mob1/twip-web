<?php
// src/AppBundle/Entity/User.php

namespace SiteBundle\Entity;


namespace SiteBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;


/**
 * @ORM\Entity
 * @ORM\Table(name="reservation")
 */
class Reservation
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id",type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;


    /**
     * @ORM\Column(name="user_id",type="integer")
     */
    public $user_id;

    /**
     * @ORM\Column(name="societe_id",type="integer", nullable=true)
     */
    public $societe_id;
    /**
     * @ORM\Column(name="adresse_id",type="integer", nullable=true)
     */
    public $adresse_id;
    /**
     * @ORM\Column(name="zone_id",type="integer", nullable=true)
     */
    public $zone_id;

    /**
     * @ORM\Column(name="coupon_id",type="integer", nullable=true)
     */
    public $coupon_id;

    /**
     * @ORM\Column(name="total_time",type="integer")
     */
    public $total_time = 0;

    /**
     * @ORM\Column(name="total_price",type="integer")
     */
    public $total_price = 0;


    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id",referencedColumnName="id", nullable=true ,onDelete="CASCADE")
     */
    private $client;

    /**
     * @ORM\ManyToOne(targetEntity="Societe")
     * @ORM\JoinColumn(name="societe_id",referencedColumnName="id", nullable=true ,onDelete="SET NULL")
     */
    private $societe;

    /**
     * @ORM\ManyToOne(targetEntity="Adresse")
     * @ORM\JoinColumn(name="adresse_id",referencedColumnName="id", nullable=true ,onDelete="SET NULL")
     */
    private $adresse;

    /**
     * @ORM\ManyToOne(targetEntity="Zone")
     * @ORM\JoinColumn(name="zone_id",referencedColumnName="id", nullable=true ,onDelete="SET NULL")
     */
    private $zone;

    /**
     * @var string
     * 0: en_attente, 1: confirmé, 2: en_cours, 3: terminé , 4: annuler
     * @ORM\Column(name="etat", type="string",nullable=true)
     */
    private $etat;

      /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="gerant_id",referencedColumnName="id", nullable=true ,onDelete="SET NULL")
     */
    private $gerant;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="employe_id",referencedColumnName="id", nullable=true ,onDelete="SET NULL")
     */
    private $employe;

    /**
     * @ORM\ManyToOne(targetEntity="Coupon")
     * @ORM\JoinColumn(name="coupon_id",referencedColumnName="id", nullable=true ,onDelete="CASCADE")
     */
    private $coupon;

    /**
     *
     * @ORM\Column(name="time", type="string", nullable=true)
     */
    private $time;

    /**
     *
     * @ORM\Column(name="date", type="date", nullable=true)
     */
    private $date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime",nullable=true)
     */
    private $created_at;


    /**
     * @ORM\OneToMany(targetEntity="ReservationHasVoitures", mappedBy="reservation")
     */
    private $reservation_voitures;

    // STATUS OF BOOKING

    const STATUS_EN_ATTENTE = 0; //en attente
    const STATUS_ACCEPTED = 1; // rdv confirmé par l'admin
    const STATUS_EN_COURS = 2; // je sais pas !!
    const STATUS_CLOSED = 3; // lavage est éffectué
    const STATUS_REFUSED = 4; // rdv annuler

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return mixed
     */
    public function getCouponId()
    {
        return $this->coupon_id;
    }

    /**
     * @param mixed $coupon_id
     */
    public function setCouponId($coupon_id)
    {
        $this->coupon_id = $coupon_id;
    }

    /**
     * @return mixed
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param mixed $client
     */
    public function setClient($client)
    {
        $this->client = $client;
    }

    /**
     * @return string
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * @param string $etat
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;
    }


    /**
     * @return mixed
     */
    public function getCoupon()
    {
        return $this->coupon;
    }

    /**
     * @param mixed $coupon
     */
    public function setCoupon($coupon)
    {
        $this->coupon = $coupon;
    }

    /**
     * @return mixed
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param mixed $time
     */
    public function setTime($time)
    {
        $this->time = $time;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param \DateTime $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getSocieteId()
    {
        return $this->societe_id;
    }

    /**
     * @param mixed $societe_id
     */
    public function setSocieteId($societe_id)
    {
        $this->societe_id = $societe_id;
    }

    /**
     * @return Societe
     */
    public function getSociete()
    {
        return $this->societe;
    }

    /**
     * @param mixed $societe
     */
    public function setSociete($societe)
    {
        $this->societe = $societe;
    }

    /**
     * @return mixed
     */
    public function getTotalTime()
    {
        return $this->total_time;
    }

    /**
     * @param mixed $total_time
     */
    public function setTotalTime($total_time)
    {
        $this->total_time = $total_time;
    }

    /**
     * @return mixed
     */
    public function getTotalPrice()
    {
        return $this->total_price;
    }

    /**
     * @param mixed $total_price
     */
    public function setTotalPrice($total_price)
    {
        $this->total_price = $total_price;
    }

    /**
     * @return mixed
     */
    public function getReservationVoitures()
    {
        return $this->reservation_voitures;
    }

    /**
     * @param mixed $reservation_voitures
     */
    public function setReservationVoitures($reservation_voitures)
    {
        $this->reservation_voitures = $reservation_voitures;
    }


    public function getEndTime()
    {
        try {

            return date("H:i", strtotime('+' . $this->getTotalTime() . ' minutes', strtotime($this->getTime())));

        } catch (\Exception $exception) {
            return "";
        }
    }

    public static $times = array(
        "08:00",
        "08:15",
        "08:30",
        "08:45",
        "09:00",
        "09:15",
        "09:30",
        "09:45",
        "10:00",
        "10:15",
        "10:30",
        "10:45",
        "11:00",
        "11:15",
        "11:30",
        "11:45",
        "12:00",
        "12:15",
        "12:30",
        "12:45",
        "13:00",
        "13:15",
        "13:30",
        "13:45",
        "14:00",
        "14:15",
        "14:30",
        "14:45",
        "15:00",
        "15:15",
        "15:30",
        "15:45",
        "16:00",
        "16:15",
        "16:30",
        "16:45",
        "17:00",
    );



    /**
     * @return mixed
     */
    public function getZoneId()
    {
        return $this->zone_id;
    }

    /**
     * @param mixed $zone_id
     */
    public function setZoneId($zone_id)
    {
        $this->zone_id = $zone_id;
    }

    /**
     * @return mixed
     */
    public function getAdresseId()
    {
        return $this->adresse_id;
    }

    /**
     * @param mixed $adresse_id
     */
    public function setAdresseId($adresse_id)
    {
        $this->adresse_id = $adresse_id;
    }

    /**
     * @return mixed
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * @param mixed $adresse
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;
    }


    /**
     * @return mixed
     */
    public function getZone()
    {
        return $this->zone;
    }

    /**
     * @param mixed $zone
     */
    public function setZone($zone)
    {
        $this->zone = $zone;
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->reservation_voitures = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set gerant
     *
     * @param \SiteBundle\Entity\User $gerant
     *
     * @return Reservation
     */
    public function setGerant(\SiteBundle\Entity\User $gerant = null)
    {
        $this->gerant = $gerant;

        return $this;
    }

    /**
     * Get gerant
     *
     * @return \SiteBundle\Entity\User
     */
    public function getGerant()
    {
        return $this->gerant;
    }

    /**
     * Set employe
     *
     * @param \SiteBundle\Entity\User $employe
     *
     * @return Reservation
     */
    public function setEmploye(\SiteBundle\Entity\User $employe = null)
    {
        $this->employe = $employe;

        return $this;
    }

    /**
     * Get employe
     *
     * @return \SiteBundle\Entity\User
     */
    public function getEmploye()
    {
        return $this->employe;
    }

    /**
     * Add reservationVoiture
     *
     * @param \SiteBundle\Entity\ReservationHasVoitures $reservationVoiture
     *
     * @return Reservation
     */
    public function addReservationVoiture(\SiteBundle\Entity\ReservationHasVoitures $reservationVoiture)
    {
        $this->reservation_voitures[] = $reservationVoiture;

        return $this;
    }

    /**
     * Remove reservationVoiture
     *
     * @param \SiteBundle\Entity\ReservationHasVoitures $reservationVoiture
     */
    public function removeReservationVoiture(\SiteBundle\Entity\ReservationHasVoitures $reservationVoiture)
    {
        $this->reservation_voitures->removeElement($reservationVoiture);
    }
}
