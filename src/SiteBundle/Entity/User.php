<?php
// src/AppBundle/Entity/User.php

namespace SiteBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="SiteBundle\Repository\UserRepository")
 * @Vich\Uploadable
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="societe_id",type="integer", nullable=true)
     */
    public $societe_id;

    /**
     * @ORM\ManyToOne(targetEntity="SiteBundle\Entity\Societe")
     * @ORM\JoinColumn(name="societe_id", referencedColumnName="id" , onDelete="CASCADE")
     **/
    private $societe;


    /**
     * @ORM\Column(name="adresse_id",type="integer", nullable=true)
     */
    public $adresse_id;

    /**
     * @ORM\ManyToOne(targetEntity="Adresse")
     * @ORM\JoinColumn(name="adresse_id", referencedColumnName="id" , onDelete="CASCADE")
     **/
    private $adresse;

    /**
     * @ORM\Column(name="gerant_id",type="integer", nullable=true)
     */
    public $gerant_id;

    /**
     * @ORM\ManyToOne(targetEntity="SiteBundle\Entity\User")
     * @ORM\JoinColumn(name="gerant_id", referencedColumnName="id" , onDelete="CASCADE")
     **/
    private $gerant;


    /**
     * @ORM\OneToMany(targetEntity="Vehicule", mappedBy="client")
     */
    private $vehicules;


    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string",nullable=true)
     */
    private $nom;


    /**
     * @var string
     *
     * @ORM\Column(name="societe_name", type="string",nullable=true)
     */
    private $societe_name = "";


    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="integer",nullable=true)
     */
    private $telephone;

    /**
     * @var string
     *
     * @ORM\Column(name="receive_sms", type="boolean")
     */
    private $receive_sms = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="receive_mail", type="boolean")
     */
    private $receive_mail = 0;


    /**
     * @ORM\Column(name="zone_id",type="integer", nullable=true)
     */
    private $zone_id;

    /**
     * @ORM\ManyToOne(targetEntity="Zone")
     * @ORM\JoinColumn(name="zone_id",referencedColumnName="id", nullable=true ,onDelete="SET NULL")
     */
    private $zone;


    public function isGranted($role)
    {
        return in_array($role, $this->getRoles());
    }


    /**
     * Get the value of societe
     */
    public function getSociete()
    {
        return $this->societe;
    }

    /**
     * Set the value of societe
     *
     * @return  self
     */
    public function setSociete($societe)
    {
        $this->societe = $societe;

        return $this;
    }

    /**
     * Get the value of domain
     *
     * @return  string
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Set the value of domain
     *
     * @param  string $domain
     *
     * @return  self
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * Get the value of telephone
     *
     * @return  string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set the value of telephone
     *
     * @param  string $telephone
     *
     * @return  self
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * @return string
     */
    public function getReceiveSms()
    {
        return $this->receive_sms;
    }

    /**
     * @param string $receive_sms
     */
    public function setReceiveSms($receive_sms)
    {
        $this->receive_sms = $receive_sms;
    }

    /**
     * @return string
     */
    public function getReceiveMail()
    {
        return $this->receive_mail;
    }

    /**
     * @param string $receive_mail
     */
    public function setReceiveMail($receive_mail)
    {
        $this->receive_mail = $receive_mail;
    }


    /**
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * @param string $adresse
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getVehicules()
    {
        return $this->vehicules;
    }

    /**
     * @param mixed $vehicules
     */
    public function setVehicules($vehicules)
    {
        $this->vehicules = $vehicules;
    }

    /**
     * @return mixed
     */
    public function getSocieteId()
    {
        return $this->societe_id;
    }

    /**
     * @param mixed $societe_id
     */
    public function setSocieteId($societe_id)
    {
        $this->societe_id = $societe_id;
    }

    public function __toString()
    {
        return $this->getNom();
    }

    /**
     * @return mixed
     */
    public function getGerant()
    {
        return $this->gerant;
    }

    /**
     * @param mixed $gerant
     */
    public function setGerant($gerant)
    {
        $this->gerant = $gerant;
    }

    /**
     * @return mixed
     */
    public function getAdresseId()
    {
        return $this->adresse_id;
    }

    /**
     * @param mixed $adresse_id
     */
    public function setAdresseId($adresse_id)
    {
        $this->adresse_id = $adresse_id;
    }

    /**
     * @return mixed
     */
    public function getGerantId()
    {
        return $this->gerant_id;
    }

    /**
     * @param mixed $gerant_id
     */
    public function setGerantId($gerant_id)
    {
        $this->gerant_id = $gerant_id;
    }

    /**
     * @return mixed
     */
    public function getZoneId()
    {
        return $this->zone_id;
    }

    /**
     * @param mixed $zone_id
     */
    public function setZoneId($zone_id)
    {
        $this->zone_id = $zone_id;
    }

    /**
     * @return mixed
     */
    public function getZone()
    {
        return $this->zone;
    }

    /**
     * @param mixed $zone
     */
    public function setZone($zone)
    {
        $this->zone = $zone;
    }

    /**
     * @return string
     */
    public function getSocieteName()
    {
        return $this->societe_name;
    }

    /**
     * @param string $societe_name
     */
    public function setSocieteName($societe_name)
    {
        $this->societe_name = $societe_name;
    }


}
