<?php
// src/AppBundle/Entity/User.php

namespace SiteBundle\Entity;


namespace SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="vehicule")
 */
class Vehicule
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id",type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @var string
     *
     * @ORM\Column(name="immatricule", type="string",nullable=true)
     */
    private $immatricule;
    /**
     * @var string
     *
     * @ORM\Column(name="modele", type="string",nullable=true)
     */
    private $modele;



    /**
     * @var string
     * mini, moto, citadine ...
     * @ORM\Column(name="type", type="string",nullable=true)
     */
    private $type;

    /**
     * @var string
     * perso, societe
     * @ORM\Column(name="categorie", type="string",nullable=true)
     */
    private $categorie;


    /**
     * @ORM\Column(name="user_id",type="integer",nullable=true)
     */
    public $user_id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id",referencedColumnName="id", nullable=true ,onDelete="CASCADE")
     */
    private $client;




    /**
     * @ORM\OneToMany(targetEntity="ReservationHasVoitures", mappedBy="voiture")
     */
    private $reservation_voitures;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updateAt", type="datetime",nullable=true)
     */
    private $updateAt;


    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $is_active = true;


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }



    /**
     * Get the value of immatricule
     *
     * @return  string
     */
    public function getImmatricule()
    {
        return $this->immatricule;
    }

    /**
     * Set the value of immatricule
     *
     * @param string $immatricule
     * @return Vehicule
     */
    public function setImmatricule($immatricule)
    {
        $this->immatricule = $immatricule;

        return $this;
    }

    /**
     * Get the value of modele
     *
     * @return  string
     */
    public function getModele()
    {
        return ($this->modele);
    }

    /**
     * Set the value of modele
     *
     * @param string $modele
     * @return Vehicule
     */
    public function setModele($modele)
    {
        $this->modele = $modele;

        return $this;
    }

    /**
     * Get the value of type
     *
     * @return  string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set the value of type
     *
     * @param string $type
     * @return Vehicule
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get the value of categrieveh
     *
     * @return  string
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Set the value of categrieveh
     *
     * @param string $categorie
     * @return Vehicule
     */
    public function setCategorie($categorie)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get the value of client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set the value of client
     *
     * @return  self
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }



    public function __toString()
    {
        return $this->modele;
    }

    /**
     * Get the value of updateAt
     *
     * @return  \DateTime
     */
    public function getUpdateAt()
    {
        return $this->updateAt;
    }

    /**
     * Set the value of updateAt
     *
     * @param  \DateTime $updateAt
     *
     * @return  self
     */
    public function setUpdateAt($updateAt)
    {
        $this->updateAt = $updateAt;

        return $this;
    }


    public $type_as_string = array(
        0 => "Mini",
        1 => "Citadine",
        2 => "Monospace/SUV",
        3 => "Camionette",
        4 => "Moto",
    );


    public $image_as_string = array(
        0 => "ic_v_mini",
        1 => "ic_v_citadine",
        2 => "ic_v_mono",
        3 => "ic_v_camion",
        4 => "ic_v_moto",
    );

    public function getTypeAsString()
    {
        try{
            return $this->type_as_string[$this->type];
        }catch (\Exception $exception){
            return "";
        }
    }

    public function getImageAsString()
    {
        try{
            return $this->image_as_string[$this->type];
        }catch (\Exception $exception){
            return "";
        }
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return mixed
     */
    public function getReservationVoitures()
    {
        return $this->reservation_voitures;
    }

    /**
     * @param mixed $reservation_voitures
     */
    public function setReservationVoitures($reservation_voitures)
    {
        $this->reservation_voitures = $reservation_voitures;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->is_active;
    }

    /**
     * @param bool $is_active
     */
    public function setIsActive($is_active)
    {
        $this->is_active = $is_active;
    }





}
