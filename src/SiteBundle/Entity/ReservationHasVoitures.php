<?php
// src/AppBundle/Entity/User.php

namespace SiteBundle\Entity;


namespace SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="reservation_has_voitures")
 */
class ReservationHasVoitures
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id",type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;


    /**
     * @ORM\ManyToOne(targetEntity="Reservation")
     * @ORM\JoinColumn(name="reservation_id",referencedColumnName="id", nullable=true ,onDelete="CASCADE")
     */
    private $reservation;

    /**
     * @ORM\ManyToOne(targetEntity="Vehicule")
     * @ORM\JoinColumn(name="voiture_id",referencedColumnName="id", nullable=true ,onDelete="CASCADE")
     */
    private $voiture;


    /**
     * @ORM\Column(name="voiture_id",type="integer")
     */
    public $voiture_id;

    /**
     * @ORM\Column(name="reservation_id",type="integer")
     */
    public $reservation_id;


    /**
     * @ORM\OneToMany(targetEntity="ReservationHasVoituresHasServices", mappedBy="reservation_has_voitures")
     */
    private $reservation_voitures_services;


    /**
     * @return mixed
     */
    public function getReservationVoituresServices()
    {
        return $this->reservation_voitures_services;
    }

    /**
     * @param mixed $reservation_voitures_services
     */
    public function setReservationVoituresServices($reservation_voitures_services)
    {
        $this->reservation_voitures_services = $reservation_voitures_services;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getReservation()
    {
        return $this->reservation;
    }

    /**
     * @param mixed $reservation
     */
    public function setReservation($reservation)
    {
        $this->reservation = $reservation;
    }

    /**
     * @return mixed
     */
    public function getVoiture()
    {
        return $this->voiture;
    }

    /**
     * @param mixed $voiture
     */
    public function setVoiture($voiture)
    {
        $this->voiture = $voiture;
    }

    /**
     * @return mixed
     */
    public function getVoitureId()
    {
        return $this->voiture_id;
    }

    /**
     * @param mixed $voiture_id
     */
    public function setVoitureId($voiture_id)
    {
        $this->voiture_id = $voiture_id;
    }

    /**
     * @return mixed
     */
    public function getReservationId()
    {
        return $this->reservation_id;
    }

    /**
     * @param mixed $reservation_id
     */
    public function setReservationId($reservation_id)
    {
        $this->reservation_id = $reservation_id;
    }


}
