<?php
// src/AppBundle/Entity/User.php

namespace SiteBundle\Entity;


namespace SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="lavage")
 */
class Lavage
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id",type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;


    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string",nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string",nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="miniPrix", type="integer")
     */
    private $miniPrix = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="miniTemps", type="integer")
     */
    private $miniTemps = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="citadinePrix", type="integer")
     */
    private $citadinePrix = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="citadineTemps", type="integer")
     */
    private $citadineTemps = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="monoPrix", type="integer")
     */
    private $monoPrix = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="monoTemps", type="integer")
     */
    private $monoTemps = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="camionPrix", type="integer")
     */
    private $camionPrix = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="camionTemps", type="integer")
     */
    private $camionTemps = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="motoPrix", type="integer")
     */
    private $motoPrix = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="motoTemps", type="integer")
     */
    private $motoTemps = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(name="activation", type="boolean")
     */
    private $activation = true;


    /**
     * autorisation constructor.
     * @param $id
     */
    public function __construct()
    {

    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getAdmin()
    {
        return $this->admin;
    }

    /**
     * @param string $admin
     */
    public function setAdmin($admin)
    {
        $this->admin = $admin;
    }

    /**
     * @return string
     */
    public function getMiniTemps()
    {
        return $this->miniTemps;
    }

    /**
     * @param string $minitemps
     */
    public function setMiniTemps($miniTemps)
    {
        $this->miniTemps = $miniTemps;
    }

    /**
     * @return string
     */
    public function getMiniPrix()
    {
        return $this->miniPrix;
    }

    /**
     * @param string $name
     */
    public function setMiniPrix($miniPrix)
    {
        $this->miniPrix = $miniPrix;
    }

    /**
     * @return string
     */
    public function getCitadinePrix()
    {
        return $this->citadinePrix;
    }

    /**
     * @param string $name
     */
    public function setCitadinePrix($citadinePrix)
    {
        $this->citadinePrix = $citadinePrix;
    }

    /**
     * @return string
     */
    public function getCitadineTemps()
    {
        return $this->citadineTemps;
    }

    /**
     * @param string $citadineTemps
     */
    public function setCitadineTemps($citadineTemps)
    {
        $this->citadineTemps = $citadineTemps;
    }


    /**
     * @return string
     */
    public function getMonoPrix()
    {
        return $this->monoPrix;
    }

    /**
     * @param string $monoPrix
     */
    public function setMonoPrix($monoPrix)
    {
        $this->monoPrix = $monoPrix;
    }

    /**
     * @return string
     */
    public function getMonoTemps()
    {
        return $this->monoTemps;
    }

    /**
     * @param string $monoPrix
     */
    public function setMonoTemps($monoTemps)
    {
        $this->monoTemps = $monoTemps;
    }

    /**
     * @return string
     */
    public function getCamionPrix()
    {
        return $this->camionPrix;
    }

    /**
     * @param string $monoPrix
     */
    public function setCamionPrix($camionPrix)
    {
        $this->camionPrix = $camionPrix;
    }

    /**
     * @return string
     */
    public function getCamionTemps()
    {
        return $this->camionTemps;
    }

    /**
     * @param string $monoPrix
     */
    public function setCamionTemps($camionTemps)
    {
        $this->camionTemps = $camionTemps;
    }

    /**
     * @return string
     */
    public function getMotoPrix()
    {
        return $this->motoPrix;
    }

    /**
     * @param string $monoPrix
     */
    public function setMotoPrix($motoPrix)
    {
        $this->motoPrix = $motoPrix;
    }

    /**
     * @return string
     */
    public function getMotoTemps()
    {
        return $this->motoTemps;
    }

    /**
     * @param string $motoTemps
     */
    public function setMotoTemps($motoTemps)
    {
        $this->motoTemps = $motoTemps;
    }

    /**
     * @return boolean
     */
    public function getActivation()
    {
        return $this->activation;
    }

    /**
     * @param string $activation
     */
    public function setActivation($activation)
    {
        $this->activation = $activation;
    }


    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    public function __toString()
    {
        return $this->name;
    }


    public function getPriceAndTimeByType($type)
    {
        $spent_time = 0;
        $total_price = 0;
        switch ($type) {
            case 0:
                $spent_time += $this->getMiniTemps();
                $total_price += $this->getMiniPrix();
                break;
            case 1:
                $spent_time += $this->getCitadineTemps();
                $total_price += $this->getCitadinePrix();
                break;
            case 2:
                $spent_time += $this->getMonoTemps();
                $total_price += $this->getMonoPrix();
                break;
            case 3:
                $spent_time += $this->getCamionTemps();
                $total_price += $this->getCamionPrix();
                break;
            case 4:
                $spent_time += $this->getMotoTemps();
                $total_price += $this->getMotoPrix();
                break;
            default:
                $spent_time += 0;
                $total_price += 0;
                break;

        }
        return array("time" => $spent_time, "price" => $total_price);

    }
}
