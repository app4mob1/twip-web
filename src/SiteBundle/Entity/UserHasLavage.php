<?php
// src/AppBundle/Entity/User.php

namespace SiteBundle\Entity;


namespace SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="user_has_lavages"))
 */
class UserHasLavage
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id",type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

       /**
     * @ORM\ManyToOne(targetEntity="Lavage")
     * @ORM\JoinColumn(name="lavage_id",referencedColumnName="id", nullable=true ,onDelete="CASCADE")
     */
    private $lavage;

     /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id",referencedColumnName="id", nullable=true ,onDelete="CASCADE")
     */
    private $User;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lavage
     *
     * @param \SiteBundle\Entity\Lavage $lavage
     *
     * @return UserHasLavage
     */
    public function setLavage(\SiteBundle\Entity\Lavage $lavage = null)
    {
        $this->lavage = $lavage;

        return $this;
    }

    /**
     * Get lavage
     *
     * @return \SiteBundle\Entity\Lavage
     */
    public function getLavage()
    {
        return $this->lavage;
    }

    /**
     * Set user
     *
     * @param \SiteBundle\Entity\User $user
     *
     * @return UserHasLavage
     */
    public function setUser(\SiteBundle\Entity\User $user = null)
    {
        $this->User = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \SiteBundle\Entity\User
     */
    public function getUser()
    {
        return $this->User;
    }
}
