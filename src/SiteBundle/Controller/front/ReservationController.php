<?php

namespace SiteBundle\Controller\front;

use Exception;
use SiteBundle\Entity\Adresse;
use SiteBundle\Entity\Coupon;
use SiteBundle\Entity\Lavage;
use SiteBundle\Entity\Participant;
use SiteBundle\Entity\Reservation;
use SiteBundle\Entity\ReservationHasVoitures;
use SiteBundle\Entity\ReservationHasVoituresHasServices;
use SiteBundle\Entity\Societe;
use SiteBundle\Entity\User;
use SiteBundle\Entity\Vehicule;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Debug\Exception\FatalErrorException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ReservationController extends Controller
{

    public function indexAction()
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $reservations = $em->getRepository('SiteBundle:Reservation')->findBy(array("user_id" => $user->getId()));
        $reservations_new = array();
        $reservations_old = array();
        /**
         * @var Reservation $reservation
         */
        foreach ($reservations as $reservation) {
            if (in_array($reservation->getEtat(), array(0, 1, 2))) $reservations_new[] = $reservation;
            elseif ($reservation->getEtat() == 3) $reservations_old[] = $reservation;
        }
        return $this->render('front/reservation/index.html.twig', array(
            "reservations_new" => $reservations_new,
            "reservations_past" => $reservations_old,
        ));
    }

    public function newAction()
    {
        $user = $this->getUser();

        if ($user != null) {
            $em = $this->getDoctrine()->getManager();

            $reservations = $em->getRepository('SiteBundle:Reservation')->findAll();
            $lavages = $em->getRepository('SiteBundle:Lavage')->findAll();
            $user = $this->getUser();
            /**
             * @var Societe $societe
             */
            $societe = $user->getSociete();
            $enable_days = array();
            if ($societe) $enable_days = $societe->getJourRes();

            $vehicules = $em->getRepository('SiteBundle:Vehicule')->findBy(array('client' => $user), array('id' => 'desc'));


            // authenticated REMEMBERED, FULLY will imply REMEMBERED (NON anonymous)

            return $this->render('front/reservation/new.html.twig', array(
                'vehicules' => $vehicules,
                'reservations' => $reservations,
                'lavages' => $lavages,
                'societe' => $societe,
                'enable_days' => $enable_days,
            ));
        }

        return $this->redirect($this->generateUrl("fos_user_security_login"));

    }


    public function createAction(Request $request)
    {

        $user = $this->getUser();

        if ($user != null) {
            $em = $this->getDoctrine()->getManager();
            $client = $this->getUser();
            $voitures = $request->get('voiture_id', array());
            $lavages = $request->get("lavage_id", array());
            $date = $request->get("date", '');
            $time = $request->get("time", '');
            $num_coupon = $request->get("coupon", '');

            /**
             * @var Coupon $coupon
             */
            $coupon = $em->getRepository("SiteBundle:Coupon")->findOneBy(array("code" => $num_coupon, "is_used" => false));
            $voitures_list = $em->getRepository("SiteBundle:Vehicule")->findBy(array("user_id" => $user->getId()));
            $services_list = $em->getRepository("SiteBundle:Lavage")->findAll();

            $voitures_ids = $this->getVoituresIds($voitures_list);
            $services_ids = $this->getServicesIds($services_list);

            if ($coupon != null) {
                $valeur_coupon = $coupon->getValeur();
            } else {
                $valeur_coupon = 0;
            }

            $errors = array();
            //empty lavage
            if (sizeof($voitures) == 0) $errors[] = "0";

            //empty service
            if (sizeof($lavages) == 0) $errors[] = "1";

            //date and time same error
            //empty date
            if ($date == "" || $time == "") $errors[] = "2";

            //empty time
            // if ($time == "") $errors[] = "3";

            //coupon valid
            if ($valeur_coupon == 0 && $num_coupon != "") $errors[] = "4";


            try {
                $voitures_options = array();
                foreach ($lavages as $lavage) {

                    $array = explode(";", $lavage);
                    if (!in_array($array[0], $voitures_ids)) $errors[] = "5";
                    if (!in_array($array[1], $services_ids)) $errors[] = "6";

                    $voitures_options[$array[0]][] = $array[1];

                }
            } catch (\Exception $exception) {
                //error parse data
                $errors[] = "-1";
            }

            if (sizeof($errors) == 0) {
                try {
                    /**
                     * @var Adresse $adresse
                     */
                    $adresse = $user->getAdresse();
                    $reservation = new Reservation();

                    $reservation->setDate(new \DateTime($date));
                    $reservation->setTime($time);
                    $reservation->setClient($user);
                    $reservation->setEtat("0");
                    $reservation->setCoupon($coupon);
                    $reservation->setSociete($user->getSociete());
                    $reservation->setAdresse($adresse);
                    if ($adresse) $reservation->setZone($adresse->getZone());
                    $reservation->setCreatedAt(new \DateTime('now', (new \DateTimeZone('Europe/Paris'))));
                    $em->persist($reservation);
                    // $em->flush();

                    $spent_time = 0;
                    $total_price = 0;
                    /**
                     * @var Vehicule $selected_vehicule
                     * @var Lavage $selected_service
                     */
                    foreach ($voitures_options as $voiture_id => $service_array) {
                        $reservation_has_voiture = new ReservationHasVoitures();
                        $reservation_has_voiture->setReservation($reservation);
                        $selected_vehicule = $this->getVehiculeById($voitures_list, $voiture_id);
                        $reservation_has_voiture->setVoiture($selected_vehicule);
                        //dump($reservation_has_voiture);
                        $em->persist($reservation_has_voiture);
                        //$em->flush();

                        foreach ($service_array as $key => $service_id) {

                            $reservation_has_voiture_has_service = new ReservationHasVoituresHasServices();
                            $selected_service = $this->getServiceById($services_list, $service_id);
                            $reservation_has_voiture_has_service->setLavage($selected_service);
                            $reservation_has_voiture_has_service->setReservationHasVoitures($reservation_has_voiture);
                            $em->persist($reservation_has_voiture_has_service);

                            switch ($selected_vehicule->getType()) {
                                case 0:
                                    $spent_time += $selected_service->getMiniTemps();
                                    $total_price += $selected_service->getMiniPrix();
                                    break;
                                case 1:
                                    $spent_time += $selected_service->getCitadineTemps();
                                    $total_price += $selected_service->getCitadinePrix();
                                    break;
                                case 2:
                                    $spent_time += $selected_service->getMonoTemps();
                                    $total_price += $selected_service->getMonoPrix();
                                    break;
                                case 3:
                                    $spent_time += $selected_service->getCamionTemps();
                                    $total_price += $selected_service->getCamionPrix();
                                    break;
                                case 4:
                                    $spent_time += $selected_service->getMotoTemps();
                                    $total_price += $selected_service->getMotoPrix();
                                    break;
                                default:
                                    $spent_time += 0;
                                    $total_price += 0;
                                    break;

                            }


                            //$em->flush();

                            //  dump($reservation_has_voiture_has_service);
                        }
                    }

                    //update coupon only whene reservztion is saved


                    $reservation->setTotalTime($spent_time);
                    $reservation->setTotalPrice($total_price);
                    $em->persist($reservation);

                    if ($coupon != null) {
                        $coupon->setIsUsed(true);
                        $em->persist($coupon);
                    }
                    $em->flush();

                    //$em->flush();
                } catch (\Exception $exception) {
                    //error save data
                    $errors[] = "-2";
                    $errors[] = $exception->getMessage();
                }
            }
        } else {
            $errors[] = "-3";
        }

        if (sizeof($errors) > 0) $result = array("message" => "error", "codes" => $errors);
        else $result = array("message" => "succes", "codes" => array(), "message" => "");

        $response = new JsonResponse($result);

        return $response;

    }


    public function getAvailableTimeByDateAction(Request $request)
    {
        /**
         * @var User $user
         */
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        $date = $request->get("date", "");
        $total_time = $request->get("total_time", 0);
        $reservations = $em->getRepository("SiteBundle:Reservation")->findBy(array("adresse_id" => $user->getAdresseId(), "date" => new \DateTime($date)));

        /**
         * @var Reservation $reservation
         */
        $used_times = array();
        foreach ($reservations as $reservation) {
            if ($reservation->getEtat() != 4) {// si la réservation n'est pas annulée
                $from = $reservation->getTime();
                $start_time = strtotime($from);
                //caluclate time end
                $to = date("H:i", strtotime('+' . $reservation->getTotalTime() . ' minutes', $start_time));

                //get all time between start and end with step 15min
                dump($from);
                dump($to);
                try {
                    $range = range(strtotime($from), strtotime($to), 15 * 60);
                } catch (\Exception $ex) {
                    $range = array();
                }

                foreach ($range as $time) {
                    $i = date("H:i", $time);
                    $used_times[] = $i;
                }


            }
        }


        $times = Reservation::$times;


        $available_times = array_diff($times, $used_times);


        //check if total time estimated can be distributed into available time
        $available_times = $this->cleanAvailableTimes($available_times, $total_time);
        $response = new JsonResponse($available_times);

        return $response;
    }

    private function getVoituresIds($voitures_list)
    {
        $result = array();
        foreach ($voitures_list as $voiture) $result[] = $voiture->getId();
        return $result;
    }

    private function getServicesIds($services_list)
    {
        $result = array();
        foreach ($services_list as $service) $result[] = $service->getId();
        return $result;
    }


    private function getVehiculeById($vehicules, $id)
    {
        foreach ($vehicules as $vehicule) if ($vehicule->getId() == $id) return $vehicule;
        return null;
    }

    private function getServiceById($services, $id)
    {
        foreach ($services as $service) if ($service->getId() == $id) return $service;
        return null;
    }

    private function cleanAvailableTimes(array $available_times, $total_time)
    {
        // dump($available_times);
        foreach ($available_times as $time) {

            $start_time = strtotime($time);
            //caluclate time end
            $to = date("H:i", strtotime('+' . $total_time . ' minutes', $start_time));

            //get all time between start and end with step 15min
            try {
                $range = range(strtotime($time), strtotime($to), 15 * 60);
            } catch (\Exception $ex) {
                $range = array();
            }
            foreach ($range as $r) {
                $i = date("H:i", $r);
                if (!in_array($i, $available_times)) {
                    $available_times = $this->deleteValueFromArray($time, $available_times);
                }
            }
        }
        //dump($available_times);
        //die;
        return $available_times;
    }

    private function deleteValueFromArray($del_val, $messages)
    {
        if (($key = array_search($del_val, $messages)) !== false) {
            unset($messages[$key]);
        }
        return $messages;
    }
}

