<?php

namespace SiteBundle\Controller\front;

use Exception;
use FOS\UserBundle\Util\TokenGenerator;
use SiteBundle\Entity\Participant;
use SiteBundle\Entity\Societe;
use SiteBundle\Entity\User;
use SiteBundle\Entity\Vehicule;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Mathielen\ImportEngine\Import;
use Symfony\Component\Finder\Finder;
use Mathielen\ImportEngine\Storage\Provider\DoctrineQueryStorageProvider;
use Mathielen\ImportEngine\ValueObject\ImportRequest;
use Mathielen\ImportEngine\ValueObject\ImportConfiguration;

class UserController extends Controller
{


    public function editAction()
    {
        $em = $this->getDoctrine()->getManager();
        /**
         * @var User $user
         * @var Societe $company
         */
        $user = $this->getUser();
        $company = $user->getSociete();
        $adresses = $company->getAdresses();
        return $this->render('front/user/edit.html.twig', array("adresses" => $adresses));
    }


    public function saveAction(Request $request)
    {
        /**
         * @var User $client
         */
        $client = $this->getUser();

//dump($request); die;
        $ids = $request->get("ids", array());
        $nom = $request->get("nom", '');
        $telephone = $request->get("telephone", '');
        $adresse_id = $request->get("adresse", '');
        $sms = $request->get("sms", '');
        $categories = $request->get("categorie", array());
        $modeles = $request->get("modele", array());
        $immatricules = $request->get("immatricule", array());
        $types = $request->get("type", array());
        if ($sms == "") $sms = false;
        else $sms = true;

        $em = $this->getDoctrine()->getManager();

        $adresse = $em->getRepository("SiteBundle:Adresse")->findOneBy(array("id" => $adresse_id));

        $client->setNom($nom);
        $client->setTelephone($telephone);
        $client->setReceiveSms($sms);
        $client->setAdresse($adresse);
        $em->persist($client);

        $vehicules = array();
        if (sizeof($ids) > 0) {
            $vehicules_collection = $em->getRepository("SiteBundle:Vehicule")->findBy(array("id" => $ids,"user_id" => $client->getId()));
            foreach ($vehicules_collection as $v) {
                $key = array_search($v->getId(), $ids);
                $vehicules[$key] = $v;
            }
        }


        if (is_array($immatricules) || is_object($immatricules)) {
            $i = 1;
            foreach ($immatricules as $immatricule) {
                if ($ids[$i] == -1) $vehicule = new Vehicule();
                else $vehicule = $vehicules[$i];

                $vehicule->setImmatricule($immatricule);
                $vehicule->setCategorie($categories[$i]);
                $vehicule->setModele($modeles[$i]);
                $vehicule->setUpdateAt(new \DateTime('now', (new \DateTimeZone('Africa/Tunis'))));
                $vehicule->setType($types[$i]);
                $vehicule->setClient($client);
                $em->persist($vehicule);
                $i++;


            }
        }
        $em->flush();
        return $this->redirectToRoute('homepage');

    }


    public function registerAction(Request $request)
    {


        //$form = $formFactory->createForm();
        $user = new User();
        $form = $this->createForm("SiteBundle\Form\RegistrationUserType", $user);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {

                $em = $this->getDoctrine()->getManager();


                // 3) Encode the password (you could also do this via Doctrine listener)
                $password = $this->get('security.password_encoder')
                    ->encodePassword($user, $user->getPlainPassword());
                $user->setPassword($password);
                $user->setUsername($user->getEmail());
                $user->setRoles(array("ROLE_CLIENT"));

                //get mail extension from email and search company by domain name
                try {
                    $domain = explode('@', $user->getEmail())[1];
                    $societe = $em->getRepository('SiteBundle:Societe')->findOneBy(array("mail" => $domain));

                } catch (\Exception $exception) {
                    $societe = null;
                }

                //check if user existe
                $checkuser = $em->getRepository("SiteBundle:User")->findOneBy(array("email" => $user->getEmail()));

                if ($checkuser != null) {
                    $form->get('email')->addError(new FormError('Email existe déjà'));
                    return $this->render('@FOSUser/Registration/register_client.html.twig', array(
                        'form' => $form->createView(),
                    ));
                }
                if ($societe != null) {
                    $user->setSociete($societe);
                }


                $receive_sms = $request->query->get("receive_sms", 0);
                $user->setReceiveSms($receive_sms);
                $user->setReceiveMail($receive_sms);


                // send confirmation token to new email
                $tokenGenerator = new TokenGenerator();
                $mailer = $this->get("fos_user.mailer");
                $user->setConfirmationToken($tokenGenerator->generateToken());
                $mailer->sendConfirmationEmailMessage($user);


                // 4) save the User!
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();


                return $this->render('@FOSUser/Registration/afterRegister.html.twig', array(
                    'message' => "Un lien vous sera envoyé pour confirmer votre inscription.",
                    'user' => $user
                ));
            }


        }

        return $this->render('@FOSUser/Registration/register_client.html.twig', array(
            'form' => $form->createView(),
        ));
    }


    public function confirmMailAction()
    {

        return $this->render('@FOSUser/Registration/confirmMail.html.twig');
    }


}

