<?php

namespace SiteBundle\Controller\front;

use SiteBundle\Entity\Produit;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Produit controller.
 *
 */
class ProduitController extends Controller
{

    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();


        $listproduits = $em->getRepository('SiteBundle:Produit')->findBy(array(), array('id' => 'desc'));

        $produits = $this->get('knp_paginator')->paginate(
            $listproduits,
            $request->query->get('page', 1)/*le numéro de la page à afficher*/,
            12/*nbre d'éléments par page*/);


        return $this->render('front/produit/index.html.twig', array(
            'produits' => $produits,
        ));
    }


    public function showAction(Produit $produit)
    {


        return $this->render('front/produit/detail.html.twig', array(
            'produit' => $produit,

        ));
    }


}
