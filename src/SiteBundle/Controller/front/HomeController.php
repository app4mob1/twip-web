<?php

namespace SiteBundle\Controller\front;

use SiteBundle\Entity\Produit;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Produit controller.
 *
 */
class HomeController extends Controller
{


    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $about_us = $em->getRepository('SiteBundle:AboutUs')->findAll();

        if ($about_us) $about_us = $about_us[0];
        return $this->render('front/home/index.html.twig', array("about_us" => $about_us));
    }


}
