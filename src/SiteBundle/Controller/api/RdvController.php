<?php

namespace SiteBundle\Controller\api;

use SiteBundle\Entity\Lavage;
use SiteBundle\Entity\Produit;
use SiteBundle\Entity\Reservation;
use SiteBundle\Entity\ReservationHasVoitures;
use SiteBundle\Entity\User;
use SiteBundle\Entity\Vehicule;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Produit controller.
 *
 */
class RdvController extends Controller
{

    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $user_id = $request->get("user_id", "");
        $date = $request->get("date", "");

        $result = array();
        /**
         * @var User $user
         */
        $user = $em->getRepository('SiteBundle:User')->findOneBy(array("id" => $user_id));
        if ($user) {

            $reservations = $em->getRepository('SiteBundle:Reservation')->findBy(array("date" => new \DateTime($date), "zone_id" => $user->getZoneId()));


            $result = array();
            /**
             * @var Reservation $reservation
             * @var User $client
             * @var ReservationHasVoitures $reservation_voitures
             * @var Vehicule $voiture
             * @var Lavage $reservation_service
             */

            $reservation_result = array();
            foreach ($reservations as $reservation) {
                $client = $reservation->getClient();
                $nb = 0;
                foreach ($reservation->getReservationVoitures() as $reservation_voitures) {
                    $array = array();
                    $voiture = $reservation_voitures->getVoiture();
                    if ($voiture != null) {
                        $array["server_id"] = $voiture->getId();
                        $array["matricule"] = $voiture->getImmatricule();
                        $array["modele"] = $voiture->getModele();
                        $array["categorie_id"] = $voiture->getType();
                        $array["categorie"] = $voiture->getTypeAsString();
                    }
                    $array["services"] = array();
                    foreach ($reservation_voitures->getReservationVoituresServices() as $reservation_service) {
                        $array["services"][] = array("name" => $reservation_service->getLavage()->getName(), "server_id" => $reservation_service->getId());
                    }
                    $reservation_result[] = $array;
                    $nb++;
                }
                $result["rdvs"][] = array(
                    "server_id" => $client->getId(),
                    "client_id" => $client->getId(),
                    "client_name" => $client->getNom(),
                    "nb_car" => $nb,
                    "total_price" => $reservation->getTotalPrice(),
                    "start_time" => $reservation->getTime(),
                    "end_time" => $reservation->getEndTime(),
                    "voitures" => $reservation_result
                );

            }

            $result["services"] = $this->getListServices($em, $user->getGerantId());
        }
        return new JsonResponse($result, 200);
    }


    public function getListServices($em, $gerant_id)
    {


        $services = $em->getRepository("SiteBundle:Lavage")->findAll();
        $result = array();
        /**
         * @var Lavage $service
         */
        foreach ($services as $service) {
            $result[] = array(
                "name" => $service->getName(),
                "miniPrix" => $service->getMiniPrix(),
                "miniTemps" => $service->getMiniTemps(),
                "citadinePrix" => $service->getCitadinePrix(),
                "citadineTemps" => $service->getCitadineTemps(),
                "monoPrix" => $service->getMonoPrix(),
                "monoTemps" => $service->getMonoTemps(),
                "camionPrix" => $service->getCamionPrix(),
                "camionTemps" => $service->getCamionTemps(),
                "motoPrix" => $service->getMotoPrix(),
                "motoTemps" => $service->getMotoTemps(),
            );
        }

        return $result;
    }


    public function return_result($message, $data)
    {
        return new JsonResponse(array(
            'message' => $message,
            'data' => $data
        ), 200);
    }


}
