<?php

namespace SiteBundle\Controller\api;

use SiteBundle\Entity\Produit;
use SiteBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Produit controller.
 *
 */
class UserController extends Controller
{

    public function loginAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $email = $request->get("email", "");
        $password = $request->get("password", "");


        $user = $em->getRepository('SiteBundle:User')->findOneBy(array("email" => $email));


        /**
         * @var User $user
         */
        if ($user && $user->hasRole("ROLE_EMPLOYE")) {
            $encoderService = $this->container->get('security.password_encoder');


            if ($match = $encoderService->isPasswordValid($user, $password)) {
                $gerant = $user->getGerant();
                $array = array(
                    "server_id" => $user->getId(),
                    "nom" => $user->getNom(),
                    "email" => $user->getEmail(),
                    "gerant_id" => $user->getGerantId(),
                    "zone_id" => $user->getZoneId(),
                    "zone" => $user->getZone(),
                );
                return $this->return_result("success", $array);

            } else
                return $this->return_result("error", array());
        }
        return $this->return_result("error", array("id" => "", "email" => ""));
    }



    public function return_result($message, $data)
    {
        return new JsonResponse(array(
            'message' => $message,
            'data' => $data
        ), 200);
    }


}
