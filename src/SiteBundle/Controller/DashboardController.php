<?php

namespace SiteBundle\Controller;

use CMEN\GoogleChartsBundle\GoogleCharts\Charts\AreaChart;
use CMEN\GoogleChartsBundle\GoogleCharts\Charts\ColumnChart;
use CMEN\GoogleChartsBundle\GoogleCharts\Charts\LineChart;
use CMEN\GoogleChartsBundle\GoogleCharts\Charts\PieChart;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DashboardController extends Controller
{
    /**
     * @Route("/chart")
     */
    public function chartAction()
    {
        return $this->render('backend/Dashboard/chart.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/index")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $lavages = $em->getRepository('SiteBundle:Lavage')->findAll();
        $lavagesService = $em->getRepository('SiteBundle:ReservationHasVoituresHasServices')->findAll();
        
        $data =   [
            ['Service', 'Nbr Services']
        ];
        $i = 1;
        foreach ($lavages as $lavage) {

            $name = $lavage->getName();
            $idLavage = $lavage->getId();
            $lavagesService = $em->getRepository('SiteBundle:ReservationHasVoituresHasServices')->findBy(array("lavage_id" => $idLavage));
            $nbr = count($lavagesService);
            $data[$i] = [$name, $nbr];
            $i++;
        }
        /*  chart 1 */
        $columnChart = $this->columnChart($data);


        /*  chart 2 */
        $coreChart = $this->coreChart($data);

        //chart 3
        $LineChart = $this->lineChart($data);

        //donutchart 4
        $donutchart = $this->donutchart($data);
        return $this->render('backend/Dashboard/index.html.twig', array(
            'columnChart' => $columnChart,
            'corechart' => $coreChart,
            'LineChart' => $LineChart,
            'donutchart' => $donutchart
        ));
    }
    public function columnChart($data)
    {
        $columnChart = new ColumnChart();
        $columnChart->getData()->setArrayToDataTable($data);
        //$columnChart->getOptions()->setTitle('Nombre des services');
        $columnChart->getOptions()->setHeight("100%");
        $columnChart->getOptions()->setWidth("100%");
        $columnChart->getOptions()->getTitleTextStyle()->setBold(true);
        $columnChart->getOptions()->getTitleTextStyle()->setColor("white");
        $columnChart->getOptions()->getTitleTextStyle()->setItalic(true);
        $columnChart->getOptions()->getTitleTextStyle()->setFontName('Arial');
        $columnChart->getOptions()->getTitleTextStyle()->setFontSize(20);
        $columnChart->getOptions()->setBackgroundColor("#01409b");
        $columnChart->getOptions()->setColors(["white", "white", "white", "white", "white", "white", "white", "white", "white", "white", "white", "white"]);
        $columnChart->getOptions()->getHAxis()->getTextStyle()->setColor("white");
       // slantedTextAngle
        $columnChart->getOptions()->getHAxis()->setSlantedTextAngle(45);
        $columnChart->getOptions()->getHAxis()->setSlantedText(true);

        // $columnChart->getOptions()->getHAxis()->setDirection(1);
        // $columnChart->getOptions()->getHAxis()->getMinorGridlines()->setColor("red");
        $columnChart->getOptions()->setHeight("1000");
        $columnChart->getOptions()->getChartArea()->setTop("5%");
        //$columnChart->getOptions()->getChartArea()->setHeight("100%");
        $columnChart->getOptions()->getVAxis()->getTextStyle()->setColor("white");

        $columnChart->getOptions()->getLegend()->setPosition('none');
        $columnChart->getOptions()->getBar()->setGroupWidth(14);
        $columnChart->getOptions()->getVAxis()->getGridlines()->setColor("none");
        $columnChart->getOptions()->getVAxis()->setBaselineColor("white");
        //$columnChart->getOptions()->getHAxis()->setDirection();
        $columnChart->getOptions()->getHAxis()->setBaseline(0);
        $columnChart->getOptions()->getVAxis()->setBaseline(0);
        $columnChart->getOptions()->getVAxis()->setFormat("0");
        //dump($axe);die();
        return $columnChart;
    }
    public function lineChart($data)
    {
        $LineChart = new LineChart();
        $LineChart->getData()->setArrayToDataTable($data);
        //$LineChart->getOptions()->setTitle('Nombre des services');
        $LineChart->getOptions()->setHeight("100%");
        $LineChart->getOptions()->setWidth("100%");
        $LineChart->getOptions()->getTitleTextStyle()->setBold(true);
        $LineChart->getOptions()->getTitleTextStyle()->setColor("white");
        $LineChart->getOptions()->getTitleTextStyle()->setItalic(true);
        $LineChart->getOptions()->getTitleTextStyle()->setFontName('Arial');
        $LineChart->getOptions()->getTitleTextStyle()->setFontSize(20);
        //  $LineChart->getOptions()->setCurveType('function');
        $LineChart->getOptions()->setLineWidth(4);
        $LineChart->getOptions()->getLegend()->setPosition('none');
        // $LineChart->getOptions()->setBackgroundColor("#01409b");
        $LineChart->getOptions()->setColors(["3366cc"]);
        $LineChart->getOptions()->setPointsVisible(true);
        $LineChart->getOptions()->setPointShape("square");
        $LineChart->getOptions()->setHeight("500");
        $LineChart->getOptions()->getChartArea()->setTop("5%");
        $LineChart->getOptions()->getVAxis()->setFormat("0");
        return $LineChart;
    }
    public function coreChart($data)
    {
        $coreChart = new AreaChart();
        $coreChart->getData()->setArrayToDataTable($data);
        //$coreChart->getOptions()->setTitle('Nombre des services');
        $coreChart->getOptions()->setHeight("100%");
        $coreChart->getOptions()->setWidth("100%");
        $coreChart->getOptions()->getTitleTextStyle()->setBold(true);
        $coreChart->getOptions()->getTitleTextStyle()->setColor("white");
        $coreChart->getOptions()->getTitleTextStyle()->setItalic(true);
        $coreChart->getOptions()->getTitleTextStyle()->setFontName('Arial');
        $coreChart->getOptions()->getTitleTextStyle()->setFontSize(20);
        //  $coreChart->getOptions()->setCurveType('function');
        $coreChart->getOptions()->setLineWidth(4);
        $coreChart->getOptions()->getLegend()->setPosition('none');
        // $coreChart->getOptions()->setBackgroundColor("#01409b");
        $coreChart->getOptions()->setColors(["pink"]);
        $coreChart->getOptions()->setHeight("500");
        $coreChart->getOptions()->getChartArea()->setTop("5%");
        $coreChart->getOptions()->getVAxis()->setFormat("0");

        return $coreChart;
    }
    public function donutchart($data)
    {
        $donutchart = new PieChart();

        $donutchart->getData()->setArrayToDataTable($data);
        //$donutchart->getOptions()->setTitle('Nombre des services');
        $donutchart->getOptions()->setHeight("100%");
        $donutchart->getOptions()->setWidth("100%");
        $donutchart->getOptions()->getTitleTextStyle()->setBold(true);
        $donutchart->getOptions()->getTitleTextStyle()->setColor("white");
        $donutchart->getOptions()->getTitleTextStyle()->setItalic(true);
        $donutchart->getOptions()->getTitleTextStyle()->setFontName('Arial');
        $donutchart->getOptions()->getTitleTextStyle()->setFontSize(20);
        $donutchart->getOptions()->setPieHole(0.4);
        $donutchart->getOptions()->setPieSliceText("none");
        $donutchart->getOptions()->getLegend()->setPosition("top");
        $donutchart->getOptions()->getChartArea()->setWidth("100%");
        $donutchart->getOptions()->getChartArea()->setHeight("80%");
        $donutchart->getOptions()->setHeight("500");
        $donutchart->getOptions()->getChartArea()->setTop("5%");
        $donutchart->getOptions()->getLegend()->setPosition("top");
        $donutchart->getOptions()->getLegend()->setAlignment("center");
        return $donutchart;
    }
}
