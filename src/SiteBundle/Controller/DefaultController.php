<?php

namespace SiteBundle\Controller;

use Exception;
use SiteBundle\Entity\Participant;
use SiteBundle\Entity\Societe;
use SiteBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Mathielen\ImportEngine\Import;
use Symfony\Component\Finder\Finder;
use Mathielen\ImportEngine\Storage\Provider\DoctrineQueryStorageProvider;
use Mathielen\ImportEngine\ValueObject\ImportRequest;
use Mathielen\ImportEngine\ValueObject\ImportConfiguration;

class DefaultController extends Controller
{


    public function baseAction()
    {
        $user = $this->getUser();
        if (!$user) {
            $token_storage = $this->container->get('security.token_storage');
            /** @var \Symfony\Component\HttpFoundation\Session\Session $session */
            $session = $GLOBALS['request']->getSession();
            $security_token = $session->get('_security_main');
            $token = unserialize($security_token);
            if ($token) {
                $token_storage->setToken($token);
                $user = $this->getUser();
                $user_repository = $this->getDoctrine()->getRepository(User::class);
                $user = $user_repository->find($user->getId());
            }
        }

        //dump($this->getUser()); die;
        if ($user->hasRole("ROLE_Client")) {
            return $this->redirect($this->generateUrl("add_profil"));
        } else if ($user->hasRole("ROLE_GERANT")) {
            return $this->redirect($this->generateUrl("listOperateur"));
        } else if ($user->hasRole("ROLE_ADMIN")) {
            return $this->redirect($this->generateUrl("listGerant"));
        }
        return $this->redirect($this->generateUrl("home"));
    }



}

