<?php

namespace SiteBundle\Controller\backend;

use SiteBundle\Entity\Coupon;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;



/**
 * Coupon controller.
 *
 */
class CouponController extends Controller
{

    /**
     * Lists all coupon entities.
     *
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $listcoupons = $em->getRepository('SiteBundle:Coupon')->findBy(array(), array('id' => 'desc'));
        $coupons  = $this->get('knp_paginator')->paginate(
            $listcoupons,
            $request->query->get('page', 1)/*le numéro de la page à afficher*/,
            10/*nbre d'éléments par page*/);

        return $this->render('backend/coupon/index.html.twig', array(
            'coupons' => $coupons,
        ));
    }

    /**
     * Creates a new coupon entity.
     *
     */
    public function newAction(Request $request)
    {
        $coupon = new Coupon();
        $form = $this->createForm('SiteBundle\Form\CouponType', $coupon);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($coupon);
            $em->flush();

            return $this->redirectToRoute('coupon_index', array('id' => $coupon->getId()));
        }

        return $this->render('backend/coupon/new.html.twig', array(
            'coupon' => $coupon,
            'form' => $form->createView(),
        ));
    }


    /**
     * Displays a form to edit an existing coupon entity.
     *
     */
    public function editAction(Request $request, Coupon $coupon)
    {

        $editForm = $this->createForm('SiteBundle\Form\CouponType', $coupon);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('coupon_edit', array('id' => $coupon->getId()));
        }

        return $this->render('backend/coupon/edit.html.twig', array(
            'coupon' => $coupon,
            'edit_form' => $editForm->createView(),
        ));
    }




    protected function configure()
    {
        // Name and description for app/console command
        $this
        ->setName('import:csv')
        ->setDescription('Import Coupon from CSV file');
    }
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Showing when the script is launched
        $now = new \DateTime();
        $output->writeln('<comment>Start : ' . $now->format('d-m-Y G:i:s') . ' ---</comment>');
        
        // Importing CSV on DB via Doctrine ORM
        $this->import($input, $output);
        
        // Showing when the script is over
        $now = new \DateTime();
        $output->writeln('<comment>End : ' . $now->format('d-m-Y G:i:s') . ' ---</comment>');
    }
    
    protected function import(InputInterface $input, OutputInterface $output)
    {
        // Getting php array of data from CSV
        $data = $this->getImport($input, $output);
        
        // Getting doctrine manager
        $em = $this->getContainer()->get('doctrine')->getManager();
        // Turning off doctrine default logs queries for saving memory
        $em->getConnection()->getConfiguration()->setSQLLogger(null);
        
        // Define the size of record, the frequency for persisting the data and the current index of records
        $size = count($data);
        $batchSize = 20;
        $i = 1;
        
        // Starting progress
        $progress = new ProgressBar($output, $size);
        $progress->start();
        
        // Processing on each row of data
        foreach($data as $row) {
            $coupon = $em->getRepository('SiteBundle:Coupon')
                       ->findOneByValeur($row['valeur']);
             
            // If the user doest not exist we create one
            if(!is_object($coupon)){
                $coupon = new Coupon();
                $coupon->setValeur($row['valeur']);
            }
            
            // Updating info
            $coupon->setValeur($row['valeur']);
            $coupon->setCode($row['code']);
      
            // Do stuff here !
  
            // Persisting the current user
            $em->persist($coupon);
            
            // Each 20 users persisted we flush everything
            if (($i % $batchSize) === 0) {
                $em->flush();
                // Detaches all objects from Doctrine for memory save
                $em->clear();
                
                // Advancing for progress display on console
                $progress->advance($batchSize);
        
                $now = new \DateTime();
                $output->writeln(' of Coupons imported ... | ' . $now->format('d-m-Y G:i:s'));
            }
            $i++;
        }
    
        // Flushing and clear data on queue
        $em->flush();
        $em->clear();
    
        // Ending the progress bar process
        $progress->finish();
    }
    protected function getImport(InputInterface $input, OutputInterface $output) 
    {
        // Getting the CSV from filesystem
        $fileName = 'web/admin/uploads/import/coupons.csv';
        
        // Using service for converting CSV to PHP Array
        $converter = $this->getContainer()->get('import.csvtoarray');
        $data = $converter->convert($fileName, ';');
        
        return $data;
    }
}

