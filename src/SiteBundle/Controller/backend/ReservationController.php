<?php

namespace SiteBundle\Controller\backend;

use SiteBundle\Entity\Reservation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Reservation controller.
 *
 */
class ReservationController extends Controller
{
    /**
     * Lists all reservation entities.
     *
     */

    public function indexAction()
    {

        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        $reservations = $em->getRepository('SiteBundle:Reservation')->findAll();
        $societes = $em->getRepository('SiteBundle:Societe')->findBy(array(), array('id' => 'desc'));

        return $this->render('backend/reservation/index.html.twig', array(
            'reservations' => $reservations,
            'societes'=> $societes
        ));
    }


    /**
     * Creates a new reservation entity.
     *
     */
    public function newAction(Request $request)
    {
        $reservation = new Reservation();
        $form = $this->createForm('SiteBundle\Form\ReservationType', $reservation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($reservation);
            $em->flush();

            return $this->redirectToRoute('reservation_show', array('id' => $reservation->getId()));
        }

        return $this->render('backend/reservation/new.html.twig', array(
            'reservation' => $reservation,
            'form' => $form->createView(),
        ));
    }





    public function accepterAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $reservation = $em->getRepository('SiteBundle:Reservation')->find($id);
        $reservation->setEtat(Reservation::STATUS_ACCEPTED);
        $em->flush();
        $reservations = $em->getRepository('SiteBundle:Reservation')->findBy(array(), array('id' => 'desc'));
        $societes = $em->getRepository('SiteBundle:Societe')->findBy(array(), array('id' => 'desc'));

        return $this->render('backend/reservation/index.html.twig', array(
            'reservations' => $reservations,
            'societes'=> $societes
        ));
    }

    public function refuserAction($id)
    {
        /**
         * @var Reservation $reservation
         */
        $em = $this->getDoctrine()->getManager();
        $reservation = $em->getRepository('SiteBundle:Reservation')->find($id);
        $reservation->setEtat(Reservation::STATUS_REFUSED);
        $em->flush();
        $reservations = $em->getRepository('SiteBundle:Reservation')->findBy(array(), array('id' => 'desc'));
        $societes = $em->getRepository('SiteBundle:Societe')->findBy(array(), array('id' => 'desc'));

        return $this->render('backend/reservation/index.html.twig', array(
            'reservations' => $reservations,
            'societes'=> $societes
        ));
    }

    /**
     * Finds and displays a reservation entity.
     *
     */
    public function showAction(Reservation $reservation)
    {
        $deleteForm = $this->createDeleteForm($reservation);

        return $this->render('backend/reservation/show.html.twig', array(
            'reservation' => $reservation,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing reservation entity.
     *
     */
    public function editAction(Request $request, Reservation $reservation)
    {
        $deleteForm = $this->createDeleteForm($reservation);
        $editForm = $this->createForm('SiteBundle\Form\ReservationType', $reservation);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('reservation_edit', array('id' => $reservation->getId()));
        }

        return $this->render('backend/reservation/edit.html.twig', array(
            'reservation' => $reservation,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a reservation entity.
     *
     */
    public function deleteAction(Request $request, Reservation $reservation)
    {
        $form = $this->createDeleteForm($reservation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($reservation);
            $em->flush();
        }

        return $this->redirectToRoute('reservation_index');
    }

    /**
     * Creates a form to delete a reservation entity.
     *
     * @param Reservation $reservation The reservation entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Reservation $reservation)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('reservation_delete', array('id' => $reservation->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }


}
