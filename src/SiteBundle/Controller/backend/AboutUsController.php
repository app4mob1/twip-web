<?php

namespace SiteBundle\Controller\backend;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use SiteBundle\Entity\Zone;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Zone controller.
 *
 */
class AboutUsController extends Controller
{


    /**
     * Displays a form to edit an existing zone entity.
     *
     */
    public function editAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        /**
         * @var ArrayCollection $about_us
         */
        $about_us = $em->getRepository('SiteBundle:AboutUs')->findAll();

        if ($about_us) $about_us = $about_us[0];
        $editForm = $this->createForm('SiteBundle\Form\AboutUsType', $about_us);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('editAboutUs');
        }

        return $this->render('backend/about_us/edit.html.twig', array(
            'about_us' => $about_us,
            'edit_form' => $editForm->createView(),
        ));
    }

}
