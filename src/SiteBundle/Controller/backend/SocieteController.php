<?php

namespace SiteBundle\Controller;

use SiteBundle\Entity\Societe;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormError;

/**
 * Societe controller.
 *
 */
class SocieteController extends Controller
{
    /**
     * Lists all societe entities.
     *
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $Societe = $em->getRepository('SiteBundle:Societe')->findAll();
       
        $this->get('session')->set('Societes',  $Societes);

        $listsocietes = $em->getRepository('SiteBundle:Societe')->findBy(array(), array('id' => 'desc'));
        $societes   = $this->get('knp_paginator')->paginate(
            $listsocietes ,
            $request->query->get('page', 1)/*le numéro de la page à afficher*/,
            10/*nbre d'éléments par page*/);
        return $this->render('backend/societe/index.html.twig', array(
            'societes' => $societes,  
        ));
    }

    /**
     * Creates a new societe entity.
     *
     */
    public function newAction(Request $request)
    {
        $societe = new Societe();
        $form = $this->createForm('SiteBundle\Form\SocieteType', $societe);
        
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($societe);

                 //check if user existe
          
                $checkmail = $em->getRepository("SiteBundle:Societe")->findOneBy(array("mail"=>$societe->getMail()));
                $checknom = $em->getRepository("SiteBundle:Societe")->findOneBy(array("nom"=>$societe->getNom()));
                if($checkmail!= null OR $checknom!=null){
                    if ($checkmail!= null) {
                        $form->get('mail')->addError(new FormError('Email extention du Société existe déjà'));
                    }
                    if ($checknom!= null) {
                        $form->get('nom')->addError(new FormError('Nom du Société existe déjà'));
                    }
                    return $this->render('societe/new.html.twig', array(
                        'form' => $form->createView(),
                    ));
                }
                else{
                    $em->flush();
                }
            

            return $this->redirectToRoute('societe_show', array('id' => $societe->getId()));
        }

        return $this->render('societe/new.html.twig', array(
            'societe' => $societe,
            'form' => $form->createView(),
        ));
    } 

    public function showAction(Societe $societe)
    {
        $deleteForm = $this->createDeleteForm($societe);

        return $this->render('societe/show.html.twig', array(
            'societe' => $societe,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing societe entity.
     *
     */
    public function editAction(Request $request, Societe $societe)
    {
        $deleteForm = $this->createDeleteForm($societe);
        $editForm = $this->createForm('SiteBundle\Form\SocieteType', $societe);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('societe_edit', array('id' => $societe->getId()));
        }

        return $this->render('societe/edit.html.twig', array(
            'societe' => $societe,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    
  }
