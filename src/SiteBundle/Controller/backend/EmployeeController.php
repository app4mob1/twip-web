<?php

namespace SiteBundle\Controller\backend;

use SiteBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * User controller.
 *
 */
class EmployeeController extends Controller
{


    /**
     * Gestion des gérant
     *
     */

    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        /****************Nombre des gérants********************/
        $users1 = $em->getRepository('SiteBundle:User')->findAll();
        $gerants = 0;
        for ($i = 0; $i < count($users1); $i++) {
            foreach ($users1[$i]->getRoles() as $key => $value) {
                if ($value == "ROLE_GERANT") {
                    $gerants++;
                }
            }
        }
        $this->get('session')->set('gerants', $gerants);


        $listeusers = $em->getRepository('SiteBundle:User')->findBy(array(), array('id' => 'desc'));

        $users = $this->get('knp_paginator')->paginate(
            $listeusers,
            $request->query->get('page', 1)/*le numéro de la page à afficher*/,
            20/*nbre d'éléments par page*/);


        return $this->render('backend/gerant/index.html.twig', array(
            'users' => $users,
        ));
    }

    public function addAction(Request $request)
    {

        /** @var $userManager UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');


        $user = new User();

        $user->setEnabled(true);
        $user->addRole("ROLE_GERANT");


        //$form = $formFactory->createForm();
        $form = $this->createForm("SiteBundle\Form\RegistrationUserType", $user);
        $form->setData($user);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {


                $user->setUsername($user->getEmail());
                $user->setEnabled(true);

                //check if user existe
                $em = $this->getDoctrine()->getManager();
                $checkuser = $em->getRepository("SiteBundle:User")->findOneBy(array("email" => $user->getEmail()));

                if ($checkuser != null) {
                    $form->get('email')->addError(new FormError('Email existe déjà'));
                    return $this->render('@FOSUser/Registration/register_gerant.html.twig', array(
                        'form' => $form->createView(),
                    ));
                } else {
                    $userManager->updateUser($user);
                }


                $url = $this->generateUrl('listGerant');
                $response = new RedirectResponse($url);


                return $response;
            }
            return $this->render('@FOSUser/Registration/register_gerant.html.twig', array(
                'form' => $form->createView(),
            ));


        }

        return $this->render('@FOSUser/Registration/register_gerant.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function editAction(Request $request, User $user)
    {

        /** @var $userManager UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');


        //$form = $formFactory->createForm();
        $form = $this->createForm("SiteBundle\Form\RegistrationGerantType", $user);
        $form->setData($user);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {


                $user->setUsername($user->getEmail());
                $user->setEnabled(true);

                //check if user existe
                $em = $this->getDoctrine()->getManager();
                $checkuser = $em->getRepository("SiteBundle:User")->findOneBy(array("email" => $user->getEmail()));

                if ($checkuser != null and $checkuser->getId() != $user->getId()) {
                    $form->get('email')->addError(new FormError('Email existe déjà'));
                    return $this->render('backend/gerant/edit.html.twig', array(
                        'form' => $form->createView(),
                        "user" => $user

                    ));
                } else {
                    $userManager->updateUser($user);
                }


                $url = $this->generateUrl('listGerant');
                $response = new RedirectResponse($url);


                return $response;
            }


        }

        return $this->render('backend/gerant/edit.html.twig', array(
            'form' => $form->createView(),
            "user" => $user
        ));
    }


}
