<?php

namespace SiteBundle\Controller\backend;

use SiteBundle\Entity\Lavage;
use SiteBundle\Entity\UserHasLavage;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;

/**
 * Lavage controller.
 *
 */
class LavageController extends Controller
{
    /**
     * Lists all lavage entities.
     *
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $listlavages = $em->getRepository('SiteBundle:Lavage')->findBy(array("activation" => true), array('id' => 'desc'));
        $lavages = $this->get('knp_paginator')->paginate(
            $listlavages,
            $request->query->get('page', 1)/*le numéro de la page à afficher*/,
            10/*nbre d'éléments par page*/);
        //$user_has_lavages = $em->getRepository("SiteBundle:UserHasLavage")->findBy(array('User'=>$user));

        return $this->render('backend/lavage/index.html.twig', array(
            'lavages' => $lavages,

        ));
    }


    public function findOneUserHasLavageAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser()->getId();
        $user_has_lavage = $em->getRepository("SiteBundle:UserHasLavage")->findOneBy(array('lavage' => $id, 'User' => $user));
        if ($user_has_lavage) {
            $statut = 1;
        } else {
            $statut = 0;
        }
        return $statut;

    }

    /**
     * Creates a new lavage entity.
     *
     */
    public function newAction(Request $request)
    {

        $lavage = new Lavage();
        $form = $this->createForm('SiteBundle\Form\LavageType', $lavage);
        $user = $this->getUser();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $lavage->setActivation(false);
            $lavage->setAdmin($user);
            $em->persist($lavage);
            $em->flush();

            return $this->redirectToRoute('lavage_index');
        }

        return $this->render('backend/lavage/new.html.twig', array(
            'lavage' => $lavage,
            'form' => $form->createView(),
        ));

    }


    /**
     * Displays a form to edit an existing lavage entity.
     *
     */
    public function editAction(Request $request, Lavage $lavage)
    {
        $deleteForm = $this->createDeleteForm($lavage);

        $editForm = $this->createForm('SiteBundle\Form\LavageType', $lavage);
        $editForm->add('description', TextareaType::class);
        $editForm->handleRequest($request);


        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('lavage_index');
        }

        return $this->render('backend/lavage/edit.html.twig', array(
            'lavage' => $lavage,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a lavage entity.
     *
     */
    public function deleteAction(Request $request, Lavage $lavage)
    {

        $em = $this->getDoctrine()->getManager();
        $lavage->setActivation(false);
        $em->flush();

        return $this->redirectToRoute('lavage_index');
    }

    /**
     * Creates a form to delete a lavage entity.
     *
     * @param Lavage $lavage The lavage entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Lavage $lavage)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('lavage_delete', array('id' => $lavage->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }


    //gestion lavage gerant
    public function myServicesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        $listlavages = $em->getRepository('SiteBundle:Lavage')->findBy(array('activation' => 1), array('id' => 'desc'));
        $lavages = $this->get('knp_paginator')->paginate(
            $listlavages,
            $request->query->get('page', 1)/*le numéro de la page à afficher*/,
            30/*nbre d'éléments par page*/);

        $user_has_lavages = $em->getRepository("SiteBundle:UserHasLavage")->findBy(array('User' => $user));

        return $this->render('backend/lavage/myServices.html.twig', array(
            'lavages' => $lavages,
            'hasLavage' => $user_has_lavages,
        ));
    }


    public function enableAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        /*
        $lavage = $em->getRepository('SiteBundle:Lavage')->find($id);
        $lavage->setActivation(1);
        $em->flush();
        $lavages = $em->getRepository('SiteBundle:Lavage')->findBy(array(), array('id' => 'desc'));
        return $this->render('backend/lavage/meslavage.html.twig', array(
            'lavages' => $lavages,
        )); */
        $lavage = $em->getRepository('SiteBundle:Lavage')->find($id);
        $user = $this->getUser();
        //dump($user); die();
        $verifExist = $em->getRepository("SiteBundle:UserHasLavage")->findBy(array('User' => $user, 'lavage' => $id));
        if (!$verifExist) {
            $userHaslavage = new UserHasLavage();
            $userHaslavage->setLavage($lavage);
            $userHaslavage->setUser($user);
            $em->persist($userHaslavage);
            $em->flush();
        }
        $lavages = $em->getRepository('SiteBundle:Lavage')->findBy(array(), array('id' => 'desc'));
        $user_has_lavages = $em->getRepository("SiteBundle:UserHasLavage")->findBy(array('User' => $user));


        return $this->render('myServices.html.twig', array(
            'lavages' => $lavages,
            'hasLavage' => $user_has_lavages,
        ));
    }

    public function disableAction($id)
    { //supprimer du table user_has_lavage
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser()->getId();
        $user_has_lavage = $em->getRepository("SiteBundle:UserHasLavage")->findOneBy(array('lavage' => $id, 'User' => $user));
        if ($user_has_lavage) {
            $em->remove($user_has_lavage);
            $em->flush();
        }
        $lavages = $em->getRepository('SiteBundle:Lavage')->findBy(array(), array('id' => 'desc'));
        $user_has_lavages = $em->getRepository("SiteBundle:UserHasLavage")->findBy(array('User' => $user));
        return $this->render('myServices.html.twig', array(
            'lavages' => $lavages,
            'hasLavage' => $user_has_lavages,
        ));
        /*  $lavage = $em->getRepository('SiteBundle:Lavage')->find($id);
         $lavage->setActivation(0);
         $em->flush();
         $lavages = $em->getRepository('SiteBundle:Lavage')->findBy(array(), array('id' => 'desc'));
         return $this->render('backend/lavage/meslavage.html.twig', array(
             'lavages' => $lavages,
         )); */

    }

}
