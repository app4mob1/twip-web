<?php

namespace SiteBundle\Controller\backend;

use SiteBundle\Entity\Zone;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Zone controller.
 *
 */
class ZoneController extends Controller
{
    /**
     * Lists all zone entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $zones = $em->getRepository('SiteBundle:Zone')->findBy(array("is_active"=> true));

        return $this->render('backend/zone/index.html.twig', array(
            'zones' => $zones,
        ));
    }

    /**
     * Creates a new zone entity.
     *
     */
    public function newAction(Request $request)
    {
        $zone = new Zone();
        $form = $this->createForm('SiteBundle\Form\ZoneType', $zone);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($zone);
            $em->flush();

            return $this->redirectToRoute('zone_index', array('id' => $zone->getId()));
        }

        return $this->render('backend/zone/new.html.twig', array(
            'zone' => $zone,
            'form' => $form->createView(),
        ));
    }



    /**
     * Displays a form to edit an existing zone entity.
     *
     */
    public function editAction(Request $request, Zone $zone)
    {
        $deleteForm = $this->createDeleteForm($zone);
        $editForm = $this->createForm('SiteBundle\Form\ZoneType', $zone);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('zone_edit', array('id' => $zone->getId()));
        }

        return $this->render('backend/zone/edit.html.twig', array(
            'zone' => $zone,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a zone entity.
     *
     */
    public function deleteAction(Request $request, Zone $zone)
    {

        $em = $this->getDoctrine()->getManager();
        $zone->setIsActive(false);
        $em->flush();


        return $this->redirectToRoute('zone_index');
    }

    /**
     * Creates a form to delete a zone entity.
     *
     * @param Zone $zone The zone entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Zone $zone)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('zone_delete', array('id' => $zone->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
