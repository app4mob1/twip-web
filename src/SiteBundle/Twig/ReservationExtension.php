<?php
/**
 * Class AppExtension
 * @package SiteBundle\Twig
 * @author Yosri Mekni
 */

namespace SiteBundle\Twig;


use Doctrine\ORM\EntityManagerInterface;
use SiteBundle\Entity\Reservation;
use SiteBundle\Entity\ReservationHasVoitures;
use SiteBundle\Entity\ReservationHasVoituresHasServices;
use SiteBundle\Entity\Vehicule;
use SiteBundle\Entity\Lavage;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class ReservationExtension extends AbstractExtension
{

    private $em ;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em ;
    }

    public function getFilters()
    {
        return [
            new TwigFilter('getServicesOfReservation', [$this, 'getServicesOfReservation']),
            new TwigFilter('getReservationVehicles', [$this, 'getReservationVehicles']),
            new TwigFilter('getVehicle', [$this, 'getVehicle']),
            new TwigFilter('getLavageName', [$this, 'getLavageName']),
        ];
    }

    public function getReservationVehicles(Reservation $reservation)
    {
        $voitures = [];
        if ($reservation)
            $voitures = $this->em->getRepository(ReservationHasVoitures::class)->findBy(array("reservation_id" => $reservation->getId()));
            //dump($voitures);die();
        return $voitures;
    }

    public function getVehicle($idVehicle)
    {
        //dump($Vehicle);
        $vehicle = $this->em->getRepository(Vehicule::class)->find($idVehicle);
//        return $idVehicle;

        return $vehicle?$vehicle->getImmatricule():null;
    }

    public function getServicesOfReservation(ReservationHasVoitures $reservation)
    {
        $services = [];
        if ($reservation)
            $services = $this->em->getRepository(ReservationHasVoituresHasServices::class)->findBy(array("reservation_has_voitures_id" => $reservation->getId()));

        return $services;
    }
    public function getLavageName($idLavage)
    {

         $lavage = $this->em->getRepository(Lavage::class)->find($idLavage);
       
         
                 return $lavage?$lavage->getName():null;
    }

}